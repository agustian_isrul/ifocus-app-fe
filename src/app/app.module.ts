import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import {
  HashLocationStrategy,
  LocationStrategy,
  PathLocationStrategy,
} from '@angular/common';
import { AppComponent } from './app.component';
import { MessageService } from 'primeng/api';
import { CardModule } from 'primeng/card';
import { ToolbarModule } from 'primeng/toolbar';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { SplitButtonModule } from 'primeng/splitbutton';
import { MenubarModule } from 'primeng/menubar';
import { PanelMenuModule } from 'primeng/panelmenu';
import { DialogModule } from 'primeng/dialog';
import { PanelModule } from 'primeng/panel';
import { CheckboxModule } from 'primeng/checkbox';
import {
  DynamicDialogModule,
  DialogService,
  DynamicDialogRef,
  DynamicDialogConfig,
} from 'primeng/dynamicdialog';
import { DividerModule } from 'primeng/divider';
import { MainmenulayoutComponent } from './layout/mainmenulayout/mainmenulayout.component';
import { NomenulayoutComponent } from './layout/nomenulayout/nomenulayout.component';
import { HomeadminComponent } from './pages/homeadmin/homeadmin.component';
import { HomeComponent } from './pages/home/home.component';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { DropdownModule } from 'primeng/dropdown';
import { BlockUIModule } from 'primeng/blockui';
import { SelectButtonModule } from 'primeng/selectbutton';
import { StepsModule } from 'primeng/steps';
import { InputNumberModule } from 'primeng/inputnumber';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { TagModule } from 'primeng/tag';
import { ChartModule } from 'primeng/chart';
import { TableModule } from 'primeng/table';
import { PickListModule } from 'primeng/picklist';
import { TooltipModule } from 'primeng/tooltip';
import { ListboxModule } from 'primeng/listbox';
import { OrderListModule } from 'primeng/orderlist';
import { CalendarModule } from 'primeng/calendar';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { NgxGaugeModule } from 'ngx-gauge';
import {TabMenuModule} from 'primeng/tabmenu';
import {InputSwitchModule} from 'primeng/inputswitch';
import { ChipModule } from 'primeng/chip';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ToastModule } from 'primeng/toast';
import {BadgeModule} from 'primeng/badge';
import { ApplicationgroupComponent } from './pages/root/applicationgroup/applicationgroup.component';
import { UsersComponent } from './pages/root/users/users.component';
import { ApplicationsComponent } from './pages/root/applications/applications.component';
import { SmtpaccountsComponent } from './pages/root/smtpaccounts/smtpaccounts.component';
import { OauthsettingsComponent } from './pages/root/oauthsettings/oauthsettings.component';
import { EventlogsComponent } from './pages/root/eventlogs/eventlogs.component';
import { ResourceusageComponent } from './pages/root/resourceusage/resourceusage.component';
import { ErrorpageComponent } from './pages/errorpage/errorpage.component';
import { BackmenulayoutComponent } from './layout/backmenulayout/backmenulayout.component';
import { LoginComponent } from './pages/login/login.component';
import { ForgotpasswordComponent } from './pages/forgotpassword/forgotpassword.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorHttpService } from './interceptors/interceptor-http.service';
import { FullmenulayoutComponent } from './layout/fullmenulayout/fullmenulayout.component';
import { TablehelperComponent } from './generic/tablehelper/tablehelper.component';
import { ApplicationdetailComponent } from './pages/root/applicationgroup/applicationdetail/applicationdetail.component';
import { UserdetailComponent } from './pages/root/users/userdetail/userdetail.component';
import { EnvServiceProvider } from './env/env.service.provider';

import { DatePipe } from '@angular/common';

import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';
import { ProfileComponent } from './pages/root/profile/profile.component';
import { ChangePasswordComponent } from './pages/root/change-password/change-password.component';
import { ResetpasswordComponent } from './pages/forgotpassword/resetpassword/resetpassword.component';

import { VerifikasiemailComponent } from './pages/forgotpassword/verifikasiemail/verifikasiemail.component';

import { SpbhomeComponent } from './pages/spb/spbhome/spbhome.component';
import { TnotifikasiComponent } from './pages/spb/spbhome/tabs/tnotifikasi/tnotifikasi.component';
import { TplanningorderComponent } from './pages/spb/spbhome/tabs/tplanningorder/tplanningorder.component';
import { TopenpoComponent } from './pages/spb/spbhome/tabs/topenpo/topenpo.component';
import { TshipmentnoticeComponent } from './pages/spb/spbhome/tabs/tshipmentnotice/tshipmentnotice.component';
import { TcapaComponent } from './pages/spb/spbhome/tabs/tcapa/tcapa.component';
import { TinvoiceComponent } from './pages/spb/spbhome/tabs/tinvoice/tinvoice.component';
import { UploadjitorderComponent } from './pages/spb/uploadjitorder/uploadjitorder.component';
import { ItemjitComponent } from './pages/spb/itemjit/itemjit.component';
import { ItemjitdetailComponent } from './pages/spb/itemjit/itemjitdetail/itemjitdetail.component';
import { PointerfaceComponent } from './pages/spb/pointerface/pointerface.component';
import { MenuComponent } from './pages/spb/menu/menu.component';
import { RequisitionComponent } from './pages/spb/requisition/requisition.component';
import { CustomerService } from './pages/spb/spbhome/tabs/topenpo/customerservice';
import { InvoicedummyService } from './pages/spb/spbhome/tabs/tinvoice/customerservice';
import { FileUploadModule } from "primeng/fileupload";
import { PonumberjitComponent } from './pages/spb/ponumberjit/ponumberjit.component';
import { SpecquoteComponent } from './pages/spb/specquote/specquote.component';
import { SpecquotedetailComponent } from './pages/spb/specquote/specquotedetail/specquotedetail.component';
import { NotificationComponent } from './pages/spb/notification/notification.component';
import { DocsetupComponent } from './pages/spb/docsetup/docsetup.component';
import { ForumcategoryComponent } from './pages/spb/forumcategory/forumcategory.component';
import { ForumthreadComponent } from './pages/spb/forumthread/forumthread.component';
import { SuppliersetupComponent } from './pages/spb/suppliersetup/suppliersetup.component';
import { MastercategoryComponent } from './pages/spb/mastercategory/mastercategory.component';
import { MastervendorComponent } from './pages/spb/mastervendor/mastervendor.component';
import { VendorproxyComponent } from './pages/spb/vendorproxy/vendorproxy.component';
import { MastervendordetailComponent } from './pages/spb/mastervendor/mastervendordetail/mastervendordetail.component';
import { VendorproxydetailComponent } from './pages/spb/vendorproxy/vendorproxydetail/vendorproxydetail.component';
import { ShipmentnoticeComponent } from './pages/spb/shipmentnotice/shipmentnotice.component';
import { ViewnotificationdetailComponent } from './pages/spb/viewnotificationdetail/viewnotificationdetail.component';
import { ChangepasswordComponent } from './pages/spb/changepassword/changepassword.component';
import { SpereportComponent } from './pages/spb/spereport/spereport.component';
import { CapaComponent } from './pages/spb/capa/capa.component';
import { SuppliermasterComponent } from './pages/spb/suppliermaster/suppliermaster.component';
import { SpecsrcComponent } from './pages/spb/specsrc/specsrc.component';
import { RegisteroutstandingComponent } from './pages/spb/registeroutstanding/registeroutstanding.component';
import { PofulfillmentComponent } from './pages/spb/pofulfillment/pofulfillment.component';
import { SubmitpoComponent } from './pages/spb/submitpo/submitpo.component';
import { PlanningorderComponent } from './pages/spb/planningorder/planningorder.component';
import { CreateplanningorderComponent } from './pages/spb/createplanningorder/createplanningorder.component';
import { JitweeklyComponent } from './pages/spb/jitweekly/jitweekly.component';
import { JitdailyComponent } from './pages/spb/jitdaily/jitdaily.component';
import { MappinguserComponent } from './pages/spb/mappinguser/mappinguser.component';
import { SpecquotationComponent } from './pages/spb/specquotation/specquotation.component';
import { ReportpaymentComponent } from './pages/spb/reportpayment/reportpayment.component';
import { PrepaymentComponent } from './pages/spb/prepayment/prepayment.component';
import { InvoiceComponent } from './pages/spb/invoice/invoice.component';
import {TreeTableModule} from 'primeng/treetable';
import {RouterModule} from '@angular/router';
import { NodeService } from './services/spb/nodeservice';
import { InvoicepoComponent } from './pages/spb/invoicepo/invoicepo.component';
import { InvoicenonpoComponent } from './pages/spb/invoicenonpo/invoicenonpo.component';
import { ListinvoiceComponent } from './pages/spb/listinvoice/listinvoice.component';
import { ListinvoiceproxyComponent } from './pages/spb/listinvoiceproxy/listinvoiceproxy.component';
import { ChoosecompanyComponent } from './pages/login/choosecompany/choosecompany.component';
import { CustomeroutlatepanelComponent } from './pages/spb/customeroutlatepanel/customeroutlatepanel.component';
import { CutoffcustomeroutletpanelComponent } from './pages/spb/cutoffcustomeroutletpanel/cutoffcustomeroutletpanel.component';

@NgModule({
  declarations: [
    AppComponent,
    MainmenulayoutComponent,
    NomenulayoutComponent,
    HomeadminComponent,
    HomeComponent,
    ApplicationgroupComponent,
    UsersComponent,
    ApplicationsComponent,
    SmtpaccountsComponent,
    OauthsettingsComponent,
    EventlogsComponent,
    ResourceusageComponent,
    ErrorpageComponent,
    BackmenulayoutComponent,
    LoginComponent,
    ForgotpasswordComponent,
    FullmenulayoutComponent,
    TablehelperComponent,
    ApplicationdetailComponent,
    UserdetailComponent,
    
    ProfileComponent,
    ChangePasswordComponent,
    ResetpasswordComponent,
   
    VerifikasiemailComponent,
 
    SpbhomeComponent,
    TnotifikasiComponent,
    TplanningorderComponent,
    TopenpoComponent,
    TshipmentnoticeComponent,
    TcapaComponent,
    TinvoiceComponent,
    UploadjitorderComponent,
    ItemjitComponent,
    ItemjitdetailComponent,
    PointerfaceComponent,
    MenuComponent,
    RequisitionComponent,
    PonumberjitComponent,
    SpecquoteComponent,
    SpecquotedetailComponent,
    NotificationComponent,
    DocsetupComponent,
    ForumcategoryComponent,
    ForumthreadComponent,
    SuppliersetupComponent,
    MastercategoryComponent,
    MastervendorComponent,
    VendorproxyComponent,
    MastervendordetailComponent,
    VendorproxydetailComponent,
    ShipmentnoticeComponent,
    ViewnotificationdetailComponent,
    ChangepasswordComponent,
    SpereportComponent,
    CapaComponent,
    SuppliermasterComponent,
    SpecsrcComponent,
    RegisteroutstandingComponent,
    PofulfillmentComponent,
    SubmitpoComponent,
    PlanningorderComponent,
    CreateplanningorderComponent,
    JitweeklyComponent,
    JitdailyComponent,
    MappinguserComponent,
    SpecquotationComponent,
    ReportpaymentComponent,
    PrepaymentComponent,
    InvoiceComponent,
    InvoicepoComponent,
    InvoicenonpoComponent,
    ListinvoiceComponent,
    ListinvoiceproxyComponent,
    ChoosecompanyComponent,
    CustomeroutlatepanelComponent,
    CutoffcustomeroutletpanelComponent,
  ],
  imports: [
    BrowserModule,
    TreeTableModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CardModule,
    ToolbarModule,
    ButtonModule,
    InputTextModule,
    SplitButtonModule,
    MenubarModule,
    PanelMenuModule,
    BreadcrumbModule,
    DropdownModule,
    ChartModule,
    BlockUIModule,
    DividerModule,
    ProgressSpinnerModule,
    ToastModule,
    BadgeModule,
    MessagesModule,
    MessageModule,
    TagModule,
    DialogModule,
    TabMenuModule,
    InputSwitchModule,
    DynamicDialogModule,
    PanelModule,
    TableModule,
    SelectButtonModule,
    StepsModule,
    InputNumberModule,
    AutoCompleteModule,
    PickListModule,
    ListboxModule,
    OrderListModule,
    CheckboxModule,
    CalendarModule,
    ChipModule,
    NgxGaugeModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    TooltipModule,
    FileUploadModule,
  ],

  // import { SelectButtonModule } from 'primeng/selectbutton';
  // import { StepsModule } from 'primeng/steps';
  // import { InputNumberModule } from 'primeng/inputnumber';
  // import { AutoCompleteModule } from 'primeng/autocomplete';

  providers: [
    EnvServiceProvider,
    DynamicDialogRef,
    DynamicDialogConfig,
    NodeService,
    MessageService,
    CustomerService,
    InvoicedummyService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorHttpService,
      multi: true,
    },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    DialogService,
    DatePipe,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
