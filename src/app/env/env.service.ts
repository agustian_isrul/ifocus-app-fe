import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnvService {

  constructor() {}
  public apiUrl = "http://localhost:3000/";
  public adminUrl = "http://localhost:3013/";
  public orderUrl = "http://localhost:3014/";
  public reportUrl = "http://localhost:3015/";
  public supmanUrl = "http://localhost:3016/";
  public bindbatchUrl = "http://localhost:3017/";
  public apiDexaUrl = "http://demo2050604.mockable.io/";
}
