import { TestBed } from '@angular/core/testing';

import { TenantmanagerService } from './tenantmanager.service';

describe('TenantmanagerService', () => {
  let service: TenantmanagerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TenantmanagerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
