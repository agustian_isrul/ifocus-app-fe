import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class TenantmanagerService {

  constructor(private service: BackendService) { }

  retriveTenantByNamein(payloads: any) {
    let obj = {payloads : payloads}
    const url = 'adm/tenants/getAllTenantsIn';
    return this.service.post(url, obj);
  }
}
