import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root',
})
export class ApplicationsService {
  allApplications: any[] = [];
  applications: any[] = [];
  constructor(private service: BackendService) {}
  retriveAppByTenant() {
    const url = 'adm/apps/appsbytenant';
    return this.service.get(url);
  }

  retriveAppByTenantAndOrgId(id: any) {
    const url = `adm/apps/retriveAppByTenantAndOrgId/${id}`;
    return this.service.get(url);
  }
  clearData() {
    this.allApplications = [];
    this.applications = [];
  }
}
