import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root',
})
export class OrganizationService {
  constructor(private service: BackendService) {}
  retriveOrgByTenant() {
    const url = 'adm/organization/allbytenantid';
    return this.service.get(url);
  }

  insertOrg(payload: any) {
    const url = 'adm/organization/addorganization';
    return this.service.post(url, payload);
  }

  retriveOrgByTenantAndOrgId(orgId: any) {
    const url = `adm/organization/retriveByTenantAndOrgId/${orgId}`;
    return this.service.get(url);
  }

  editOrg(payload: any) {
    const url = 'adm/organization/editorganization';
    return this.service.post(url, payload);
  }

  deleteOrg(payload: any) {
    const url = 'adm/organization/deleteorganization';
    return this.service.post(url, payload);
  }
}
