import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService } from 'angular-web-storage';
import { BehaviorSubject, Observable } from 'rxjs';
import { BackendResponse } from '../interfaces/backend-response';
import { BackendService } from './backend.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  response: BehaviorSubject<BackendResponse | null> =
    new BehaviorSubject<BackendResponse | null>(null);
  private authenticated: boolean = false;
  private tokenStorage = new BehaviorSubject('');
  sharedMessage = this.tokenStorage.asObservable();
  constructor(
    private sessionStorage: SessionStorageService,
    private backend: BackendService,
    private router: Router
  ) {}

  isLoggedIn(): boolean {
    console.log('Apakah sudah is auth ? ' + this.authenticated);
    const token = sessionStorage.getItem('accesstoken');
    if (token !== null) this.authenticated = true;
    return this.authenticated;
  }
  loggedOut(): void {
    this.sessionStorage.clear();
    this.authenticated = false;
  }

  // whoAmi(){
  //   const url = 'adm/auth/who';
  //   return this.backend.get(url);
  // }
  whoAmi() {
    const url = 'adm/auth/who';
    return this.backend.get(url);
  }

  whoAmiInitialize(payload: string) {
    const url = 'integrate/api/initialtoken';
    return this.backend.post(url, { tokenNaked: payload });
  }

  loginAdmin(payload: any) {
    const url = 'adm/auth/signviaadmin';
    return this.backend.post(url, payload);
  }

  changeAppLication(payload: number) {
    const url = 'adm/auth/changeapp';
    return this.backend.post(url, { appid: payload });
  }
  setAuthStatus(status: boolean): void {
    this.authenticated = status;
  }
  setToken(tokenIn: string): void {
    this.tokenStorage.next(tokenIn);
  }
}
