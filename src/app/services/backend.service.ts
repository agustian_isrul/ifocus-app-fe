import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
// import { environment } from '@environtment';
import { catchError } from 'rxjs/operators';
import { EnvService } from '../env/env.service';
@Injectable({
  providedIn: 'root',
})
export class BackendService {
  constructor(private httpClient: HttpClient, private environment: EnvService) {}
  post(path: string, payload: any, authorized?: boolean): Observable<any> {
    const url = this.environment.apiUrl + path;
    return this.httpClient
      .post(url, payload)
      .pipe(catchError(this.handleError));
  }
  get(path: string, authorized?: boolean): Observable<any> {
    const url = this.environment.apiUrl + path;
    return this.httpClient.get(url).pipe(catchError(this.handleError));
  }

  put(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.apiUrl + path;
    return this.httpClient.put(url, payload).pipe(catchError(this.handleError));
  }

  patch(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.apiUrl + path;
    return this.httpClient
      .patch(url, payload)
      .pipe(catchError(this.handleError));
  }

  delete(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.apiUrl + path;
    return this.httpClient
      .delete(url, payload)
      .pipe(catchError(this.handleError));
  }

  adminPost(path: string, payload: any, authorized?: boolean): Observable<any> {
    const url = this.environment.adminUrl + path;
    return this.httpClient
      .post(url, payload)
      .pipe(catchError(this.handleError));
  }
  adminGet(path: string, authorized?: boolean): Observable<any> {
    const url = this.environment.adminUrl + path;
    return this.httpClient.get(url).pipe(catchError(this.handleError));
  }

  adminPut(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.adminUrl + path;
    return this.httpClient.put(url, payload).pipe(catchError(this.handleError));
  }

  adminPatch(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.adminUrl + path;
    return this.httpClient
      .patch(url, payload)
      .pipe(catchError(this.handleError));
  }

  adminDelete(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.adminUrl + path;
    return this.httpClient
      .delete(url, payload)
      .pipe(catchError(this.handleError));
  }

  orderPost(path: string, payload: any, authorized?: boolean): Observable<any> {
    const url = this.environment.orderUrl + path;
    return this.httpClient
      .post(url, payload)
      .pipe(catchError(this.handleError));
  }
  orderGet(path: string, authorized?: boolean): Observable<any> {
    const url = this.environment.orderUrl + path;
    return this.httpClient.get(url).pipe(catchError(this.handleError));
  }

  orderPut(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.orderUrl + path;
    return this.httpClient.put(url, payload).pipe(catchError(this.handleError));
  }

  orderPatch(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.orderUrl + path;
    return this.httpClient
      .patch(url, payload)
      .pipe(catchError(this.handleError));
  }

  orderDelete(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.orderUrl + path;
    return this.httpClient
      .delete(url, payload)
      .pipe(catchError(this.handleError));
  }
  reportPost(path: string, payload: any, authorized?: boolean): Observable<any> {
    const url = this.environment.reportUrl + path;
    return this.httpClient
      .post(url, payload)
      .pipe(catchError(this.handleError));
  }
  reportGet(path: string, authorized?: boolean): Observable<any> {
    const url = this.environment.reportUrl + path;
    return this.httpClient.get(url).pipe(catchError(this.handleError));
  }

  reportPut(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.reportUrl + path;
    return this.httpClient.put(url, payload).pipe(catchError(this.handleError));
  }

  reportPatch(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.reportUrl + path;
    return this.httpClient
      .patch(url, payload)
      .pipe(catchError(this.handleError));
  }

  reportDelete(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.reportUrl + path;
    return this.httpClient
      .delete(url, payload)
      .pipe(catchError(this.handleError));
  }
  supmanPost(path: string, payload: any, authorized?: boolean): Observable<any> {
    const url = this.environment.supmanUrl + path;
    return this.httpClient
      .post(url, payload)
      .pipe(catchError(this.handleError));
  }
  supmanGet(path: string, authorized?: boolean): Observable<any> {
    const url = this.environment.supmanUrl + path;
    return this.httpClient.get(url).pipe(catchError(this.handleError));
  }

  supmanPut(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.supmanUrl + path;
    return this.httpClient.put(url, payload).pipe(catchError(this.handleError));
  }

  supmanPatch(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.supmanUrl + path;
    return this.httpClient
      .patch(url, payload)
      .pipe(catchError(this.handleError));
  }

  supmanDelete(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.supmanUrl + path;
    return this.httpClient
      .delete(url, payload)
      .pipe(catchError(this.handleError));
  }
  bindbatchPost(path: string, payload: any, authorized?: boolean): Observable<any> {
    const url = this.environment.bindbatchUrl + path;
    return this.httpClient
      .post(url, payload)
      .pipe(catchError(this.handleError));
  }
  bindbatchGet(path: string, authorized?: boolean): Observable<any> {
    const url = this.environment.bindbatchUrl + path;
    return this.httpClient.get(url).pipe(catchError(this.handleError));
  }

  bindbatchPut(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.bindbatchUrl + path;
    return this.httpClient.put(url, payload).pipe(catchError(this.handleError));
  }

  bindbatchPatch(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.bindbatchUrl + path;
    return this.httpClient
      .patch(url, payload)
      .pipe(catchError(this.handleError));
  }

  bindbatchDelete(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.bindbatchUrl + path;
    return this.httpClient
      .delete(url, payload)
      .pipe(catchError(this.handleError));
  }
  // tslint:disable-next-line:typedef
  handleError(error: HttpErrorResponse) {
    console.log('error occured ', error);
    return throwError(error);
  }

  getDexa(path: string, authorized?: boolean): Observable<any> {
    const url = this.environment.apiDexaUrl + path;
    return this.httpClient.get(url).pipe(catchError(this.handleError));
  }
}
