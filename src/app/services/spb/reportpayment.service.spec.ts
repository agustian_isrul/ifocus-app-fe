import { TestBed } from '@angular/core/testing';

import { ReportpaymentService } from './reportpayment.service';

describe('ReportpaymentService', () => {
  let service: ReportpaymentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReportpaymentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
