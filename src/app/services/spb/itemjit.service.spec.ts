import { TestBed } from '@angular/core/testing';

import { ItemjitService } from './itemjit.service';

describe('ItemjitService', () => {
  let service: ItemjitService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemjitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
