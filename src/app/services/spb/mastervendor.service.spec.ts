import { TestBed } from '@angular/core/testing';

import { MastervendorService } from './mastervendor.service';

describe('MastervendorService', () => {
  let service: MastervendorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MastervendorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
