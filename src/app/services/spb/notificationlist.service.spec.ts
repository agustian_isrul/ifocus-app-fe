import { TestBed } from '@angular/core/testing';

import { NotifikasilistService } from './notificationlist.service';

describe('NotificationlistService', () => {
  let service: NotifikasilistService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NotifikasilistService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
