import { StringValueToken } from "html2canvas/dist/types/css/syntax/tokenizer";
import * as XLSX from "xlsx";

export interface Country {
    name?: string;
    code?: string;
  }
  
  export interface Representative {
    name?: string;
    image?: string;
    namee?: string;
    nameee?: string;
    nameeee?: string;
  }

  export interface Customerr {
    line?: number;
    item?: string;
    itemd?: string;
    uom?: string;
    location: string;
    currency: string;
    cprice: string;
    cqty: string;
    cas: string;
    pr: string;
    po: string;
    lines: string;
    status: string;
    representative?: Representative;
  }
  
  export interface Customer {
    representative?: Representative;
    supplier?: string;
    prnumber?: number;
    line?: string;
    date?: string;
    approval?: string;
    item?: string;
    itemd?: string;
    qty?: number;
    uom?: string;
    location?: string;
    noteb?: string;
    notes?: string;
    manufaktur?: string;
    currency?: string;
    cqty?: string;
    arrival?: string;
    lines?: string;
    cprice?: string;
    dest?: string;
    last?: string;
    name: string;
    id?: number;
    country?: Country;
    company?: string;
    status?: string;
  }

  export interface IDepartment {
    Id:Number;
    Name:string;
  }

  export interface Capa {
    id?: number;
    name?: string;
    document?: string;
    date?: string | Date;
    subject?: string;
    responsedate?: string;
    response?: string;
    status?: string;
    price?: number;
  }

  export interface Product {
    id?:string;
    code?:string;
    name?:string;
    namee?:string;
    description?:string;
    price?:number;
    quantity?:number;
    inventoryStatus?:string;
    category?:string;
    image?:string;
    rating?:number;
    date?: string | Date;
    orders?: string;
}

    export interface Source {
        source?:string;
        supplier?:string;
        type?:string;
        subject?:string;
        sender?:string;
        recipent?:string;
        creation?:string;
        created?:string;
        message?:string;
    }

    const getFileName = (name: string) => {
      let timeSpan = new Date().toISOString();
      let sheetName = name || "ExportResult";
      let fileName = `${sheetName}-${timeSpan}`;
      return {
        sheetName,
        fileName
      };
    };
    export class TableUtil {
      static exportTableToExcel(tableId: string, name?: string) {
        let { sheetName, fileName } = getFileName(name);
        let targetTableElm = document.getElementById(tableId);
        let wb = XLSX.utils.table_to_book(targetTableElm, <XLSX.Table2SheetOpts>{
          sheet: sheetName
        });
        XLSX.writeFile(wb, `${fileName}.xlsx`);
      }
    
      static exportArrayToExcel(arr: any[], name?: string) {
        let { sheetName, fileName } = getFileName(name);
    
        var wb = XLSX.utils.book_new();
        var ws = XLSX.utils.json_to_sheet(arr);
        XLSX.utils.book_append_sheet(wb, ws, sheetName);
        XLSX.writeFile(wb, `${fileName}.xlsx`);
      }
    }