import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerPanelService {

  constructor(private service: BackendService) {}

  getAll() {
    const url = 'spb/customers/getallcustomers';
    return this.service.adminGet(url);
  }

  getCustomerNyCompanyIdAreaCustomerName() {
    const url = 'getCustomer';
    return this.service.getDexa(url);
  }

  getCutCustomerPanel() {
    const url = 'getCutCustomerPanel';
    return this.service.getDexa(url);
  }

  

 
}
