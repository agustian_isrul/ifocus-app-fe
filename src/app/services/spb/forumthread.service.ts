import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class ForumthreadService {
  constructor(private service: BackendService) {}

  retriveItem(id: string) {
    const url = 'adm/forumthread/' + id;
    return this.service.get(url);
  }

  insertforumthread(payload: any) {
    const url = 'adm/forumthread/insertbyadmin';
    return this.service.post(url, payload);
  }

  updateforumthread(payload: any) {
    const url = 'adm/forumthread/updateforumthread';
    return this.service.post(url, payload);
  }

  updatebyAdminActive(payload: any) {
    const url = 'adm/umanager/updatebyadminactive';
    return this.service.post(url, payload);
  }
  
  retriveforumthread() {
    const url = 'adm/forumthread/retriveforumthread';
    return this.service.get(url);
  }

  retriveforumthreadById(id: string) {
    const url = 'adm/forumthread/retriveforumthreadbyid/' + id;
    return this.service.get(url);
  }

  deleteforumthread(payload: any) {
    // console.log("HAPUS "+JSON.stringify(payload));
    const url = `adm/forumthread/deleteforumthread/${payload.forumthread.id} `;
    return this.service.get(url);
  }
}
