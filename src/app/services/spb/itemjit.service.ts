import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class ItemjitService {
  constructor(private service: BackendService) {}

  retriveItem(id: string) {
    const url = 'adm/itemjit/' + id;
    return this.service.get(url);
  }

  insertItemJit(payload: any) {
    const url = 'adm/itemjit/insertbyadmin';
    return this.service.post(url, payload);
  }

  updateItemJit(payload: any) {
    const url = 'adm/itemjit/updateitemjit';
    return this.service.post(url, payload);
  }

  updatebyAdminActive(payload: any) {
    const url = 'adm/umanager/updatebyadminactive';
    return this.service.post(url, payload);
  }
  
  retriveItemJit() {
    const url = 'adm/itemjit/retriveitemjit';
    return this.service.get(url);
  }

  retriveItemJitById(id: string) {
    const url = 'adm/itemjit/retriveitemjitbyid/' + id;
    return this.service.get(url);
  }

  deleteItemJit(payload: any) {
    // console.log("HAPUS "+JSON.stringify(payload));
    const url = `adm/itemjit/deleteitemjit/${payload.itemjit.id} `;
    return this.service.get(url);
  }
}
