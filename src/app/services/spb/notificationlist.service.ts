import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationlistService {

  constructor(private service: BackendService) {}

  getAll() {
    const url = 'spb/notification/getnotifbytenant';
    return this.service.adminGet(url);
  }

  getModulType() {
    const url = 'spb/notification/getallmodules';
    return this.service.adminGet(url);
  }
  
  getStatusType() {
    const url = 'spb/notification/getStatusType/';
    return this.service.adminGet(url);
  }
  
  getAllModulNotification() {
    const url = 'spb/notification/getallmodules'
    return this.service.adminGet(url);
  }
  
}
