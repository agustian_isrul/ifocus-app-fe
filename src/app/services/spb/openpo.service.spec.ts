import { TestBed } from '@angular/core/testing';

import { OpenpoService } from './openpo.service';

describe('OpenpoService', () => {
  let service: OpenpoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OpenpoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
