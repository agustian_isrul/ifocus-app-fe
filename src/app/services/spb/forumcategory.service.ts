import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class ForumcategoryService {
  constructor(private service: BackendService) {}

  retriveItem(id: string) {
    const url = 'adm/forumcategory/' + id;
    return this.service.get(url);
  }

  insertforumcategory(payload: any) {
    const url = 'adm/forumcategory/insertbyadmin';
    return this.service.post(url, payload);
  }

  updateforumcategory(payload: any) {
    const url = 'adm/forumcategory/updateforumcategory';
    return this.service.post(url, payload);
  }

  updatebyAdminActive(payload: any) {
    const url = 'adm/umanager/updatebyadminactive';
    return this.service.post(url, payload);
  }
  
  retriveforumcategory() {
    const url = 'adm/forumcategory/retriveforumcategory';
    return this.service.get(url);
  }

  retriveforumcategoryById(id: string) {
    const url = 'adm/forumcategory/retriveforumcategorybyid/' + id;
    return this.service.get(url);
  }

  deleteforumcategory(payload: any) {
    // console.log("HAPUS "+JSON.stringify(payload));
    const url = `adm/forumcategory/deleteforumcategory/${payload.forumcategory.id} `;
    return this.service.get(url);
  }
}
