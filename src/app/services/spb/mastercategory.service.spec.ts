import { TestBed } from '@angular/core/testing';

import { MastercategoryService } from './mastercategory.service';

describe('MastercategoryService', () => {
  let service: MastercategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MastercategoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
