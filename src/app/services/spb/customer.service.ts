import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Customer, Customerr, Product } from './customer';
import { TreeNode } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  status: string[] = ['OUTOFSTOCK', 'INSTOCK', 'LOWSTOCK'];

    productNames: string[] = [
        "PT Ancol Terang Metal Printing Industri", 
        "PT Uniflex Kemansidah", 
        "PT Total Pack Indonesia"
    ];

  constructor(
    private http: HttpClient
    ) { }
  getCustomersLarge() {
    return this.http.get<any>('assets/customers-large.json')
        .toPromise()
        .then(res => <Customer[]>res.data)
        .then(data => { return data; });
}
getCustomersSmall() {
  return this.http.get<any>('assets/customers-small.json')
      .toPromise()
      .then(res => <Customer[]>res.data)
      .then(data => { return data; });
}

getCustomersMedium() {
  return this.http.get<any>('assets/customer-medium.json')
      .toPromise()
      .then(res => <Customer[]>res.data)
      .then(data => { return data; });
}

getProductsSmall() {
  return this.http.get<any>('assets/products-small.json')
  .toPromise()
  .then(res => <Product[]>res.data)
  .then(data => { return data; });
}

getCustomersMediumm() {
  return this.http.get<any>('assets/customer-new.json')
  .toPromise()
  .then(res => <Customerr[]>res.data)
  .then(data => { return data; });
}

}
export class NodeService {

  constructor(private http: HttpClient) { }

  getFilesystem() {
  return this.http.get<any>('assets/file-system.json')
    .toPromise()
    .then(res => <TreeNode[]>res.data);
  }

  getLazyFilesystem() {
  return this.http.get<any>('assets/filesystem-lazy.json')
    .toPromise()
    .then(res => <TreeNode[]>res.data);
  }
}
