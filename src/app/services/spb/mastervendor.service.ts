import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class MastervendorService {
  constructor(private service: BackendService) {}

  retriveItem(id: string) {
    const url = 'adm/mastervendor/' + id;
    return this.service.get(url);
  }

  insertmastervendor(payload: any) {
    const url = 'adm/mastervendor/insertbyadmin';
    return this.service.post(url, payload);
  }

  updatemastervendor(payload: any) {
    const url = 'adm/mastervendor/updatemastervendor';
    return this.service.post(url, payload);
  }

  updatebyAdminActive(payload: any) {
    const url = 'adm/umanager/updatebyadminactive';
    return this.service.post(url, payload);
  }
  
  retrivemastervendor() {
    const url = 'adm/mastervendor/retrivemastervendor';
    return this.service.get(url);
  }

  retrivemastervendorById(id: string) {
    const url = 'adm/mastervendor/retrivemastervendorbyid/' + id;
    return this.service.get(url);
  }

  deletemastervendor(payload: any) {
    // console.log("HAPUS "+JSON.stringify(payload));
    const url = `adm/mastervendor/deletemastervendor/${payload.mastervendor.id} `;
    return this.service.get(url);
  }
}
