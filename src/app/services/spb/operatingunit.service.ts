import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class OperatingunitService {

  constructor(private service: BackendService) { }
  getDBInstance(payload:any) {
    const url = 'spb/operationunit/getdbinstancebytnname/'+payload;
    return this.service.adminGet(url);
  }

  getSupplierByDBINstance(payload:any) {
    const url = 'spb/suppliers/getsuppliersbydbInstance/'+payload;
    return this.service.adminGet(url);
  }
  
  getStatusType() {
    const url = 'spb/operationunit/getStatusType/';
    return this.service.adminGet(url);
  }
  
  getAllModuloperationunit() {
    const url = 'spb/operationunit/getallmodules'
    return this.service.adminGet(url);
  }
}
