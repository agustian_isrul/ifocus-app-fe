import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class DocsetupService {
  constructor(private service: BackendService) {}

  retriveItem(id: string) {
    const url = 'adm/docsetup/' + id;
    return this.service.get(url);
  }

  insertdocsetup(payload: any) {
    const url = 'adm/docsetup/insertbyadmin';
    return this.service.post(url, payload);
  }

  updatedocsetup(payload: any) {
    const url = 'adm/docsetup/updatedocsetup';
    return this.service.post(url, payload);
  }

  updatebyAdminActive(payload: any) {
    const url = 'adm/umanager/updatebyadminactive';
    return this.service.post(url, payload);
  }
  
  retrivedocsetup() {
    const url = 'adm/docsetup/retrivedocsetup';
    return this.service.get(url);
  }

  retrivedocsetupById(id: string) {
    const url = 'adm/docsetup/retrivedocsetupbyid/' + id;
    return this.service.get(url);
  }

  deletedocsetup(payload: any) {
    // console.log("HAPUS "+JSON.stringify(payload));
    const url = `adm/docsetup/deletedocsetup/${payload.docsetup.id} `;
    return this.service.get(url);
  }
}
