import { TestBed } from '@angular/core/testing';

import { SuppliersetupService } from './suppliersetup.service';

describe('SuppliersetupService', () => {
  let service: SuppliersetupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SuppliersetupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
