import { TestBed } from '@angular/core/testing';

import { SpecquoteService } from './specquote.service';

describe('SpecquoteService', () => {
  let service: SpecquoteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SpecquoteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
