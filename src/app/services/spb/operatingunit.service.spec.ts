import { TestBed } from '@angular/core/testing';

import { OperatingunitService } from './operatingunit.service';

describe('OperatingunitService', () => {
  let service: OperatingunitService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OperatingunitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
