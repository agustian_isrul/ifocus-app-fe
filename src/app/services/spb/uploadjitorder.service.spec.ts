import { TestBed } from '@angular/core/testing';

import { UploadjitorderService } from './uploadjitorder.service';

describe('UploadjitorderService', () => {
  let service: UploadjitorderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UploadjitorderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
