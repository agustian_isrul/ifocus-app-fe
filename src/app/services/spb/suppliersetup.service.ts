import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class SuppliersetupService {
  constructor(private service: BackendService) {}

  retriveItem(id: string) {
    const url = 'adm/suppliersetup/' + id;
    return this.service.get(url);
  }

  insertsuppliersetup(payload: any) {
    const url = 'adm/suppliersetup/insertbyadmin';
    return this.service.post(url, payload);
  }

  updatesuppliersetup(payload: any) {
    const url = 'adm/suppliersetup/updatesuppliersetup';
    return this.service.post(url, payload);
  }

  updatebyAdminActive(payload: any) {
    const url = 'adm/umanager/updatebyadminactive';
    return this.service.post(url, payload);
  }
  
  retrivesuppliersetup() {
    const url = 'adm/suppliersetup/retrivesuppliersetup';
    return this.service.get(url);
  }

  retrivesuppliersetupById(id: string) {
    const url = 'adm/suppliersetup/retrivesuppliersetupbyid/' + id;
    return this.service.get(url);
  }

  deletesuppliersetup(payload: any) {
    // console.log("HAPUS "+JSON.stringify(payload));
    const url = `adm/suppliersetup/deletesuppliersetup/${payload.suppliersetup.id} `;
    return this.service.get(url);
  }
}

