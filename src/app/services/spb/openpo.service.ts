import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';
import { HttpClient } from '@angular/common/http';
import { Customer } from './openpodummy';

@Injectable({
  providedIn: 'root'
})
export class OpenpoService {

  constructor(private service: BackendService, private http: HttpClient) {}
  getAll() {
    const url = 'spb/openpolist/getAll';
    return this.service.get(url);
  }

  getModulType() {
    const url = 'spb/openpolist/getModulType/';
    return this.service.get(url);
  }
  
  getStatusType() {
    const url = 'spb/openpolist/getStatusType/';
    return this.service.get(url);
  }
  
  getByModulStatus(payload: any) {
    const url = 'spb/openpolist/getByModul/' + payload;
    return this.service.get(url);
  }
  
  getCustomersLarge() {
    return this.http.get<any>('assets/customers-large.json')
        .toPromise()
        .then(res => <Customer[]>res.data)
        .then(data => { return data; });
}

}
