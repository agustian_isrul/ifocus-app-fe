import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class PointerfaceService {
  constructor(private service: BackendService) {}

  retriveItem(id: string) {
    const url = 'adm/pointerface/' + id;
    return this.service.get(url);
  }

  insertpointerface(payload: any) {
    const url = 'adm/pointerface/insertbyadmin';
    return this.service.post(url, payload);
  }

  updatepointerface(payload: any) {
    const url = 'adm/pointerface/updatepointerface';
    return this.service.post(url, payload);
  }

  updatebyAdminActive(payload: any) {
    const url = 'adm/umanager/updatebyadminactive';
    return this.service.post(url, payload);
  }
  
  retrivepointerface() {
    const url = 'adm/pointerface/retrivepointerface';
    return this.service.get(url);
  }

  retrivepointerfaceById(id: string) {
    const url = 'adm/pointerface/retrivepointerfacebyid/' + id;
    return this.service.get(url);
  }

  deletepointerface(payload: any) {
    // console.log("HAPUS "+JSON.stringify(payload));
    const url = `adm/pointerface/deletepointerface/${payload.pointerface.id} `;
    return this.service.get(url);
  }
}
