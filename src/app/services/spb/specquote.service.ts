import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class SpecquoteService {
  constructor(private service: BackendService) {}

  retriveItem(id: string) {
    const url = 'adm/specquote/' + id;
    return this.service.get(url);
  }

  insertspecquote(payload: any) {
    const url = 'adm/specquote/insertbyadmin';
    return this.service.post(url, payload);
  }

  updatespecquote(payload: any) {
    const url = 'adm/specquote/updatespecquote';
    return this.service.post(url, payload);
  }

  updatebyAdminActive(payload: any) {
    const url = 'adm/umanager/updatebyadminactive';
    return this.service.post(url, payload);
  }
  
  retrivespecquote() {
    const url = 'adm/specquote/retrivespecquote';
    return this.service.get(url);
  }

  retrivespecquoteById(id: string) {
    const url = 'adm/specquote/retrivespecquotebyid/' + id;
    return this.service.get(url);
  }

  deletespecquote(payload: any) {
    // console.log("HAPUS "+JSON.stringify(payload));
    const url = `adm/specquote/deletespecquote/${payload.specquote.id} `;
    return this.service.get(url);
  }
}

