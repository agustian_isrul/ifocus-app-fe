import { TestBed } from '@angular/core/testing';

import { DocsetupService } from './docsetup.service';

describe('DocsetupService', () => {
  let service: DocsetupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DocsetupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
