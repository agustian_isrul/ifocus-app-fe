import { TestBed } from '@angular/core/testing';

import { PonumberjitService } from './ponumberjit.service';

describe('PonumberjitService', () => {
  let service: PonumberjitService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PonumberjitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
