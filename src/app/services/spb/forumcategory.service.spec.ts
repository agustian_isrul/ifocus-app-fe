import { TestBed } from '@angular/core/testing';

import { ForumcategoryService } from './forumcategory.service';

describe('ForumcategoryService', () => {
  let service: ForumcategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ForumcategoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
