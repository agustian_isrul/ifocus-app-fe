import { TestBed } from '@angular/core/testing';

import { PointerfaceService } from './pointerface.service';

describe('PointerfaceService', () => {
  let service: PointerfaceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PointerfaceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
