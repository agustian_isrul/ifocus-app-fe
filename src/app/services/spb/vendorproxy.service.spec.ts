import { TestBed } from '@angular/core/testing';

import { VendorproxyService } from './vendorproxy.service';

describe('VendorproxyService', () => {
  let service: VendorproxyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VendorproxyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
