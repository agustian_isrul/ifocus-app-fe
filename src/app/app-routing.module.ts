import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GuardGuard } from './guard/guard.guard';
import { BackmenulayoutComponent } from './layout/backmenulayout/backmenulayout.component';
import { FullmenulayoutComponent } from './layout/fullmenulayout/fullmenulayout.component';
import { MainmenulayoutComponent } from './layout/mainmenulayout/mainmenulayout.component';
import { NomenulayoutComponent } from './layout/nomenulayout/nomenulayout.component';
import { ErrorpageComponent } from './pages/errorpage/errorpage.component';
import { HomeComponent } from './pages/home/home.component';
import { HomeadminComponent } from './pages/homeadmin/homeadmin.component';
import { LoginComponent } from './pages/login/login.component';
import { ForgotpasswordComponent } from './pages/forgotpassword/forgotpassword.component';
import { ApplicationdetailComponent } from './pages/root/applicationgroup/applicationdetail/applicationdetail.component';
import { ApplicationgroupComponent } from './pages/root/applicationgroup/applicationgroup.component';
import { ApplicationsComponent } from './pages/root/applications/applications.component';
import { EventlogsComponent } from './pages/root/eventlogs/eventlogs.component';
import { OauthsettingsComponent } from './pages/root/oauthsettings/oauthsettings.component';
import { ResourceusageComponent } from './pages/root/resourceusage/resourceusage.component';
import { SmtpaccountsComponent } from './pages/root/smtpaccounts/smtpaccounts.component';
import { UserdetailComponent } from './pages/root/users/userdetail/userdetail.component';
import { UsersComponent } from './pages/root/users/users.component';

import { ProfileComponent } from './pages/root/profile/profile.component';
import { ResetpasswordComponent } from './pages/forgotpassword/resetpassword/resetpassword.component';

import { VerifikasiemailComponent } from './pages/forgotpassword/verifikasiemail/verifikasiemail.component';

import { SpbhomeComponent } from './pages/spb/spbhome/spbhome.component';
import { TnotifikasiComponent } from './pages/spb/spbhome/tabs/tnotifikasi/tnotifikasi.component';
import { TplanningorderComponent } from './pages/spb/spbhome/tabs/tplanningorder/tplanningorder.component';
import { TopenpoComponent } from './pages/spb/spbhome/tabs/topenpo/topenpo.component';
import { TshipmentnoticeComponent } from './pages/spb/spbhome/tabs/tshipmentnotice/tshipmentnotice.component';
import { TcapaComponent } from './pages/spb/spbhome/tabs/tcapa/tcapa.component';
import { TinvoiceComponent } from './pages/spb/spbhome/tabs/tinvoice/tinvoice.component';
import { ItemjitComponent } from './pages/spb/itemjit/itemjit.component';
import { ItemjitdetailComponent } from './pages/spb/itemjit/itemjitdetail/itemjitdetail.component';
import { MenuComponent } from './pages/spb/menu/menu.component';
import { PointerfaceComponent } from './pages/spb/pointerface/pointerface.component';
import { RequisitionComponent } from './pages/spb/requisition/requisition.component';
import { UploadjitorderComponent } from './pages/spb/uploadjitorder/uploadjitorder.component';
import { PonumberjitComponent } from './pages/spb/ponumberjit/ponumberjit.component';
import { SpecquoteComponent } from './pages/spb/specquote/specquote.component';
import { SpecquotedetailComponent } from './pages/spb/specquote/specquotedetail/specquotedetail.component';
import { NotificationComponent } from './pages/spb/notification/notification.component';
import { DocsetupComponent } from './pages/spb/docsetup/docsetup.component';
import { ForumcategoryComponent } from './pages/spb/forumcategory/forumcategory.component';
import { ForumthreadComponent } from './pages/spb/forumthread/forumthread.component';
import { SuppliersetupComponent } from './pages/spb/suppliersetup/suppliersetup.component';
import { MastervendorComponent } from './pages/spb/mastervendor/mastervendor.component';
import { VendorproxyComponent } from './pages/spb/vendorproxy/vendorproxy.component';
import { MastervendordetailComponent } from './pages/spb/mastervendor/mastervendordetail/mastervendordetail.component';
import { VendorproxydetailComponent } from './pages/spb/vendorproxy/vendorproxydetail/vendorproxydetail.component';
import { ShipmentnoticeComponent } from './pages/spb/shipmentnotice/shipmentnotice.component';
import { ChangepasswordComponent } from './pages/spb/changepassword/changepassword.component';
import { CapaComponent } from './pages/spb/capa/capa.component';
import { SpecsrcComponent } from './pages/spb/specsrc/specsrc.component';
import { MappinguserComponent } from './pages/spb/mappinguser/mappinguser.component';
import { SpereportComponent } from './pages/spb/spereport/spereport.component';
import { SuppliermasterComponent } from './pages/spb/suppliermaster/suppliermaster.component';
import { SpecquotationComponent } from './pages/spb/specquotation/specquotation.component';
import { CreateplanningorderComponent } from './pages/spb/createplanningorder/createplanningorder.component';
import { RegisteroutstandingComponent } from './pages/spb/registeroutstanding/registeroutstanding.component';
import { PlanningorderComponent } from './pages/spb/planningorder/planningorder.component';
import { SubmitpoComponent } from './pages/spb/submitpo/submitpo.component';
import { PofulfillmentComponent } from './pages/spb/pofulfillment/pofulfillment.component';
import { PrepaymentComponent } from './pages/spb/prepayment/prepayment.component';
import { InvoiceComponent } from './pages/spb/invoice/invoice.component';
import { ReportpaymentComponent } from './pages/spb/reportpayment/reportpayment.component';
import { InvoicepoComponent } from './pages/spb/invoicepo/invoicepo.component';
import { InvoicenonpoComponent } from './pages/spb/invoicenonpo/invoicenonpo.component';
import { ListinvoiceComponent } from './pages/spb/listinvoice/listinvoice.component';
import { ListinvoiceproxyComponent } from './pages/spb/listinvoiceproxy/listinvoiceproxy.component';
import { ViewnotificationdetailComponent } from './pages/spb/viewnotificationdetail/viewnotificationdetail.component';
import { ChoosecompanyComponent } from './pages/login/choosecompany/choosecompany.component';
import { CustomeroutlatepanelComponent } from './pages/spb/customeroutlatepanel/customeroutlatepanel.component';
import { CutoffcustomeroutletpanelComponent } from './pages/spb/cutoffcustomeroutletpanel/cutoffcustomeroutletpanel.component';

const routes: Routes = [
  { path: '', redirectTo: 'auth/login', pathMatch: 'full' },
  {
    path: 'mgm',
    canActivate: [GuardGuard],
    component: MainmenulayoutComponent,
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'profile', component: ProfileComponent },
      {
        path: 'user',
        canActivate: [GuardGuard],
        children: [
          { path: 'userslist', component: UsersComponent },
          {
            path: 'userslist',
            children: [
              { path: 'detail', component: UserdetailComponent },
              { path: 'detail#/:id', component: UserdetailComponent },
            ],
          },
        ],
      },
      {
        path: 'adm',
        canActivate: [GuardGuard],
        children: [
          { path: 'menulist', component: MenuComponent },
          { path: 'jitorderlist', component: UploadjitorderComponent },
          { path: 'ponumberjit', component: PonumberjitComponent },

        ],
      },
      {
        path: 'acl',
        canActivate: [GuardGuard],
        children: [
          { path: 'grouplist', component: ApplicationgroupComponent },
          {
            path: 'grouplist',
            children: [
              { path: 'detail', component: ApplicationdetailComponent },
              { path: 'detail#/:id', component: ApplicationdetailComponent },
            ],
          },
        ],
      },
      {
        path: 'system',
        canActivate: [GuardGuard],
        children: [
          { path: 'sysparam', component: SpbhomeComponent },
          {
            path: 'sysparam',
            children: [
              { path: 'detail', component: SpbhomeComponent },
              { path: 'detail#/:id', component: SpbhomeComponent },
            ],
          },
        ],
      },
      {
        path: 'settings',
        canActivate: [GuardGuard],
        children: [
          { path: 'bicadminlist', component: SpbhomeComponent },
          {
            path: 'bicadminlist',
            children: [
              { path: 'detail', component: SpbhomeComponent },
              { path: 'detail#/:id', component: SpbhomeComponent },
            ],
          },

          { path: 'notificationlist', component: ViewnotificationdetailComponent },

          { path: 'submitpo', component: SubmitpoComponent },

          { path: 'register', component: RegisteroutstandingComponent },
          
          { path: 'createpo', component: CreateplanningorderComponent },

          { path: 'shipment', component: ShipmentnoticeComponent },

          { path: 'planningorder', component: PlanningorderComponent },

          { path: 'specsrc', component: SpecsrcComponent },
          { path: 'sysparamslist', component: PofulfillmentComponent },
          {
            path: 'sysparamslist',
            children: [
              { path: 'detail', component: SpbhomeComponent },
              { path: 'detail#/:id', component: SpbhomeComponent },
            ],
          },
          { path: 'netmgm', component: SpbhomeComponent },
          { path: 'proxyaliaslist', component: SpbhomeComponent },
          {
            path: 'proxyaliaslist',
            children: [
              { path: 'detail', component: SpbhomeComponent },
              { path: 'detail#/:id', component: SpbhomeComponent },
            ],
          },
          {
            path: 'proxymaintenancelist',
            component: SpbhomeComponent,
          },
          {
            path: 'proxymaintenancelist',
            children: [
              { path: 'detail', component: SpbhomeComponent },
              {
                path: 'detail#/:id',
                component: SpbhomeComponent,
              },
            ],
          },
          { path: 'trxcostlist', component: ItemjitComponent },
          {
            path: 'trxcostlist',
            children: [
              { path: 'detail', component: ItemjitdetailComponent },
              {
                path: 'detail#/:id',
                component: ItemjitdetailComponent,
              },
            ],
          },
          { path: 'specquotelist', component: SpecquoteComponent },
          {
            path: 'specquotelist',
            children: [
              { path: 'detail', component: SpecquotedetailComponent },
              {
                path: 'detail#/:id',
                component: SpecquotedetailComponent,
              },
            ],
          },
          { path: 'pointerface', component: PointerfaceComponent },
          // {
          //   path: 'pointerface',
          //   children: [
          //     { path: 'detail', component: PointerfaceDetailComponent },
          //     {
          //       path: 'detail#/:id',
          //       component: ItemjitdetailComponent,
          //     },
          //   ],
          // },
          {
            path: 'prefunddashboard',
            component: SpbhomeComponent,
          },
          {
            path: 'forumcategorylist',
            component: ForumcategoryComponent,
          },
          {
            path: 'forumthreadlist',
            component: ForumthreadComponent,
          },
          {
            path: 'suppliersetuplist',
            component: SuppliersetupComponent,
          },
          {
            path: 'vendorlist',
            component: MastervendorComponent,
          },  
          {
            path: 'vendorlist',
            children: [
              { path: 'detail', component: MastervendordetailComponent },
              {
                path: 'detail#/:id',
                component: MastervendordetailComponent,
              },
            ],
          },
          { path: 'mappinguser', component: MappinguserComponent },
          {
            path: 'vendorproxylist',
            component: VendorproxyComponent,
          },{
            path: 'vendorproxylist',
            children: [
              { path: 'detail', component: VendorproxydetailComponent },
              {
                path: 'detail#/:id',
                component: VendorproxydetailComponent,
              },
            ],
          },
          {
            path: 'notificationlist',
            component: NotificationComponent,
          },
          {
            path: 'docsetuplist',
            component: DocsetupComponent,
          },
          {
            path: 'prefunddashboard',
            children: [
              {
                path: 'detail',
                component: SpbhomeComponent,
              },
              {
                path: 'detail#/:id',
                component: SpbhomeComponent,
              },
            ],
          },
          { path: 'prefundmgmlist', component: SpbhomeComponent },
          {
            path: 'prefundmgmlist',
            children: [
              { path: 'detail', component: SpbhomeComponent },
              { path: 'detail#/:id', component: SpbhomeComponent },
            ],
          },
          { path: 'branchlist', component: SpbhomeComponent },
          {
            path: 'branchlist',
            children: [
              { path: 'detail', component: SpbhomeComponent },
              { path: 'detail#/:id', component: SpbhomeComponent },
            ],
          },
          { path: 'requisition', component: RequisitionComponent },
          { path: 'smtpconfig', component: SpbhomeComponent },
        ],
      },
      {
        path: 'master',
        canActivate: [GuardGuard],
        children: [
          { path: 'customeroutlet', component: CustomeroutlatepanelComponent },
          { path: 'cutoffcustomeroutlet', component: CutoffcustomeroutletpanelComponent },
          { path: 'prepaymentlist', component: PrepaymentComponent },
          { path: 'paymentlist', component: ReportpaymentComponent },
          { path: 'invoicelist', component: InvoiceComponent },
          { path: 'channeltypelist', component: SpbhomeComponent },
          {
            path: 'channeltypelist',
            children: [
              { path: 'detail', component: SpbhomeComponent },
              { path: 'detail#/:id', component: SpbhomeComponent },
            ],
          },
          { path: 'proxytypelist', component: SpecquotationComponent },
          {
            path: 'proxytypelist',
            children: [
              { path: 'detail', component: SpbhomeComponent },
              { path: 'detail#/:id', component: SpbhomeComponent },
            ],
          },
          { path: 'limitlist', component: ChangepasswordComponent },
          {
            path: 'limitlist',
            children: [
              { path: 'detail', component: SpbhomeComponent },
              { path: 'detail#/:id', component: SpbhomeComponent },
            ],
          },
          { path: 'accounttype', component: SuppliermasterComponent },
          {
            path: 'accounttype',
            children: [
              { path: 'detail', component: SpbhomeComponent },
              {
                path: 'detail#/:id',
                component: SpbhomeComponent,
              },
            ],
          },
          { path: 'customertype', component: CapaComponent },
          {
            path: 'customertype',
            children: [
              { path: 'detail', component: SpbhomeComponent },
              {
                path: 'detail#/:id',
                component: SpbhomeComponent,
              },
            ],
          },
          { path: 'idtype', component: SpbhomeComponent },
          {
            path: 'idtype',
            children: [
              { path: 'detail', component: SpbhomeComponent },
              { path: 'detail#/:id', component: SpbhomeComponent },
            ],
          },
          { path: 'resident', component: SpereportComponent },
          {
            path: 'resident',
            children: [
              { path: 'detail', component: SpbhomeComponent },
              {
                path: 'detail#/:id',
                component: SpbhomeComponent,
              },
            ],
          },
          {
            path: 'adminnotificationlist',
            component: SpbhomeComponent,
          },
          {
            path: 'adminnotificationlist',
            children: [
              { path: 'detail', component: SpbhomeComponent },
              {
                path: 'detail#/:id',
                component: SpbhomeComponent,
              },
            ],
          },
        ],
      },
      {
        path: 'monitor',
        canActivate: [GuardGuard],
        children: [
          {
            path: 'invoicepo',component: InvoicepoComponent,
          },
          {
            path: 'invoicenonpo',component: InvoicenonpoComponent,
          },
          {
            path: 'listinvoice',component: ListinvoiceComponent,
          },
          {
            path: 'listinvoiceproxy',component: ListinvoiceproxyComponent,
          },
          {
            path: 'logmonitor',
            component: SpbhomeComponent,
          },
          {
            path: 'actionevent',
            component: SpbhomeComponent,
          },
          {
            path: 'systemevent',
            component: SpbhomeComponent,
          },
          {
            path: 'inboundlogsystem',
            component: SpbhomeComponent,
          },
          {
            path: 'outboundlogsystem',
            component: SpbhomeComponent,
          },
          {
            path: 'eventlog',
            component: SpbhomeComponent,
          },
        ],
      },
      {
        path: 'report',
        canActivate: [GuardGuard],
        children: [
          { path: 'transaction', component: SpbhomeComponent },
          {
            path: 'transaction',
            children: [
              { path: 'detail', component: SpbhomeComponent },
              { path: 'detail#/:id', component: SpbhomeComponent },
            ],
          },
        ],
      },
    ],
  },
  {
    path: 'nopage',
    component: BackmenulayoutComponent,
    children: [{ path: '404', component: ErrorpageComponent }],
  },
  {
    path: 'auth',
    data: { title: 'Login' },
    component: NomenulayoutComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'forgotpassword', component: ForgotpasswordComponent },
      { path: 'resetpassword/:id', component: ResetpasswordComponent },
      { path: 'verifikasiemail/:id', component: VerifikasiemailComponent },
      // { path: 'choose', component: ChoosecompanyComponent }
    ],
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
