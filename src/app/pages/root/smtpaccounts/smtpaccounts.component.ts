import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-smtpaccounts',
  templateUrl: './smtpaccounts.component.html',
  styleUrls: ['./smtpaccounts.component.scss']
})
export class SmtpaccountsComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  constructor() { }

  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/'};
    this.breadcrumbs = [
      { label: 'SMTP Management' }
    ];
  }

}
