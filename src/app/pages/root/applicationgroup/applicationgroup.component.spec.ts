import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationgroupComponent } from './applicationgroup.component';

describe('ApplicationgroupComponent', () => {
  let component: ApplicationgroupComponent;
  let fixture: ComponentFixture<ApplicationgroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApplicationgroupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationgroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
