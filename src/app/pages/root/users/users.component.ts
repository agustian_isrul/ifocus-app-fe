import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, Message, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { UsermanagerService } from 'src/app/services/root/usermanager.service';
import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  display = false;
  scrollheight:any ="400px"
  viewDisplay = false;
  viewApprove = false;
  selectedUser: any = {};
  isGroup = false;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  userlist: any[] = [];
 
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = '';
  usrheader: any = [
    { label: 'Name', sort: 'fullname' },
    { label: 'User ID', sort: 'userid' },
    { label: 'Group Menu', sort: 'groupname' },
    { label: 'Type', sort: 'levelname' },
    { label: 'Active', sort: 'active' },
    { label: 'Created At', sort: 'created_at' },
  ];
  usrcolname: any = [
    'fullname',
    'userid',
    'groupname',
    'levelname',
    'active',
    'created_at',
    'updated_at',
    'description',
    'groupname',
    'bioemailactive',
    'bioemailactivecc',
    // 'active',
    // 'total_attempt',
  ];
  usrcolhalign: any = [
    '',
    '',
    '',
    '',
    // 'p-text-center',
    'p-text-center',
    'p-text-center',
    // 'p-text-center',
  ];
  usrcolwidth: any = [
    { width: '250px' },
    { width: '250px' },
    { width: '150px' },
    { width: '170px' },
    { width: '100px' },
    { width: '200px' },
  ];
  usrfieldformat: any =['','','','','',{code:'D', format:'dd-MMM-yy, hh:mm:ss'}];




  usrlistmenubtn: any = [
    {label: 'Add Data', icon: 'pi pi-user', routerLink: ['detail']}
    // {label: 'Table Setting', icon: 'pi pi-cog', command: () => {
    //   this.previewfields();
    // }},
  ]


  // orgcollinghref:any = {'url':'#','label':'Application'}
  usractionbtn: any = [1, 1, 1, 1, 1, 1];
  // usraddbtn = { route: 'detail', label: 'Add Data' };
  usraddbtn = { route: 'detail', label: 'Commands' };
  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private userService: UsermanagerService,
    private aclMenuService : AclmenucheckerService,
    private router: Router
  ) {
  //   this.items = [
  //     {label: 'Select Columns', icon: 'pi pi-refresh', command: () => {
  //         this.update();
  //     }},
  //     {label: 'Filter', icon: 'pi pi-times', command: () => {
  //         this.delete();
  //     }},
  //     {label: 'Rows Per Page', icon: 'pi pi-times', command: () => {
  //         this.delete();
  //     }},
  //     {label: 'Format', icon: 'pi pi-times', command: () => {
  //         this.delete();
  //     }},
  //     {label: 'Flashback', icon: 'pi pi-times', command: () => {
  //         this.delete();
  //     }},
  //     {label: 'Save Report', icon: 'pi pi-times', command: () => {
  //         this.delete();
  //     }},
  //     {label: 'Reset', icon: 'pi pi-times', command: () => {
  //         this.delete();
  //     }},
  //     {label: 'Help', icon: 'pi pi-times', command: () => {
  //         this.delete();
  //     }},
  //     {label: 'Download', icon: 'pi pi-times', command: () => {
  //         this.delete();
  //     }},
     
  //     // {label: 'Setup', icon: 'pi pi-cog', routerLink: ['/setup']}
  // ];
  }

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'User' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.aclMenuService.setAllMenus(this.userInfo.sidemenus).then(data => {
        console.log("MENU ALL ACL Set");
        this.aclMenuService.getAclMenu(this.router.url).then(dataacl =>{
          if(JSON.stringify(dataacl.acl) === "{}"){
            console.log("No ACL Founded")
          } else {
            console.log("ACL Founded");
            console.log(dataacl.acl);
            this.usractionbtn[0] = dataacl.acl.create;
              this.usractionbtn[1] = dataacl.acl.read;
              this.usractionbtn[2] = dataacl.acl.update;
              this.usractionbtn[3] = dataacl.acl.delete;
              this.usractionbtn[4] = dataacl.acl.view;
              this.usractionbtn[5] = dataacl.acl.approval;
          }
        });
      });
      
    });
    this.refreshingUser();
  }
  refreshingUser() {
    this.isFetching = true;
    this.authservice.whoAmi().subscribe((data: BackendResponse) => {
      // console.log(">>>>>>> "+JSON.stringify(data));
      let objtmp = {
        fullname: 'Chandra',
        userid: '10001',
        active: 'No records',
        groupname: 'Creator',
        created_at: '2021-12-07T08:03:28.000Z',
        updated_at: '2021-12-07T08:03:28.000Z',
        description: 'User Test1',
        bioemailactive: 'chandra@mii.co.id',
        bioemailactivecc: 'director@mii.co.id',
      };
      this.userlist = [];
      this.userlist.push(objtmp);


      if ((data.status = 200)) {
        this.userService.retriveUsers().subscribe((orgall: BackendResponse) => {
          // console.log('>>>>>>> ' + JSON.stringify(orgall));
          this.userlist = orgall.data;
          if (this.userlist.length < 1) {
            let objtmp = {
              fullname: 'No records',
              userid: 'No records',
              active: 'No records',
              groupname: 'No records',
              created_at: 'No records',
              updated_at: 'No records',
              description: 'No records',
              bioemailactive: 'No records',
            };
            this.userlist = [];
            this.userlist.push(objtmp);
          }
          this.isFetching = false;
        });
      }
    });
  }
  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedUser = data;
  }

  viewData(data: any) {
    console.log(data);
    this.viewDisplay = true;
    this.selectedUser = data;
  }

  approvalData(data: any) {
    // console.log(data);
    this.viewApprove = true;
    this.selectedUser = data;
    
  }
  approvalSubmit(status){
    // console.log(this.selectedUser);
    // console.log(status);
    let payload = {
      id: this.selectedUser.id,
      oldactive:this.selectedUser.active,
      isactive: status,
      idapproval: this.selectedUser.idapproval
    };
    // console.log('>>>>>>>> payload ' + JSON.stringify(payload));
    this.userService
      .updatebyAdminActive(payload)
      .subscribe((result: BackendResponse) => {
        // console.log(">>>>>>>> return "+JSON.stringify(result));
        if (result.status === 200) {
          this.refreshingUser();
          this.viewApprove = false;
        }
      });
  }

  deleteUser() {
    console.log(this.selectedUser);
    let user = this.selectedUser;
    const payload = { user };
    this.userService
      .deleteUserByAdmin(payload)
      .subscribe((resp: BackendResponse) => {
        console.log(resp);
        if (resp.status === 200) {
          this.showTopSuccess(resp.data);
        }
        this.display = false;
        this.refreshingUser();
      });
  }

  



  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }

  save(severity: string) {
    this.messageService.add({severity:severity, summary:'Success', detail:'Data Saved'});
  }

  update() {
      this.messageService.add({severity:'success', summary:'Success', detail:'Data Updated'});
  }

  delete() {
      this.messageService.add({severity:'success', summary:'Success', detail:'Data Deleted'});
  }
}
