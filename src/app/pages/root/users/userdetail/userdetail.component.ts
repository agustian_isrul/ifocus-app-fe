import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AuthService } from 'src/app/services/auth.service';
import { Location } from '@angular/common';
import { OrganizationService } from 'src/app/services/root/organization.service';
import { UsermanagerService } from 'src/app/services/root/usermanager.service';
import { FilterService } from 'primeng/api';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { GroupServiceService } from 'src/app/services/root/group-service.service';
import { ForgotpasswordService } from 'src/app/services/forgotpassword/forgotpassword.service';
import { OperatingunitService } from 'src/app/services/spb/operatingunit.service';

@Component({
  selector: 'app-userdetail',
  templateUrl: './userdetail.component.html',
  styleUrls: ['./userdetail.component.scss'],
})
export class UserdetailComponent implements OnInit {
  extraInfo: any = {};
  isEdit: boolean = false;
  userId: any = null;
  stateOptions: any[] = [];
  stateOptionsEdit: any[] = [];

  empOrSup: any[] = [];
  empsupClicked: any = 1;
  empList: any[] = [];
  empSuggest: any = {};
  selectedComp: any;
  companyList:any = [];
  supList: any[] = [];
  supSuggest: any = {};

  supplierselectlist: any[] = [];
  selectedSpl: any;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo: any = {};
  selectedApps: any = [];
  tokenID: string = '';
  groupForm!: FormGroup;
  submitted = false;
  orgsData: any[] = [];
  appInfoActive: any = {};
  orgSuggest: any = {};
  user: any = {};
  formatedOrg: any[] = [];
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private umService: UsermanagerService,
    private authservice: AuthService,
    private groupService: GroupServiceService,
    private filterService: FilterService,
    private messageService: MessageService,
    private location: Location,
    private verifikasiService: ForgotpasswordService,
    private operatingunitService: OperatingunitService
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    let checkurl = this.extraInfo.indexOf('%23') !== -1 ? true : false;
    console.log('>>>>>>>>>>> ' + this.extraInfo);
    console.log(checkurl);
    if (checkurl) this.isEdit = true;
  }

  ngOnInit(): void {
    let currdate=new Date();
    let expdate=new Date();
    expdate.setFullYear(expdate.getFullYear() + 10);
    // this.currentdate = this.datepipe.transform(currdate, 'EEEE, dd-MMM-yy')
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      {
        label: 'Users Management',
        command: (event) => {
          this.location.back();
        },
        url: '',
      },
      { label: this.isEdit ? 'Edit data' : 'Add data' },
    ];
    this.companyList=[]
    this.umService.getAllTenant().subscribe((tenantResult: BackendResponse) => {
      // console.log("Tenant Results ", tenantResult);
      this.companyList = tenantResult.data;
    });

    // this.stateOptions = [
    //   { label: 'Direct activated', value: 1 },
    //   { label: 'Activision link', value: 0 },
    // ];
    this.stateOptions = [
      { label: 'Active', value: 1 },
      { label: 'Deactive', value: 0 },
    ];
    this.stateOptionsEdit = [
      { label: 'Active', value: 1 },
      { label: 'Deactive', value: 0 },
    ];
    this.empOrSup = [{ label: 'Employee', value: 1 },
    { label: 'Supplier', value: 2 },]
    
    // {code:"3123123", label:"NANANG SUPARLAN", email:"nanang@gmail.com"},
    // {code:"3123124", label:"HETTY KOES", email:"hetty@gmail.com"},
    // {code:"3123125", label:"RINTO HASARAP", email:"rinto@gmail.com"},
    // {code:"3123126", label:"BETTY BANDEL", email:"beba@gmail.com"}
    this.empList = [];
    // {code:"123", label:"ANCOL 1"},{code:"124", label:"ARISTOCRAT CIPTA MANDIRI"},{code:"126", label:"PT AGUNG JAYA"},{code:"127", label:"ANCOL 2"}
    this.supList = [];

    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      this.groupForm = this.formBuilder.group({
        username: ['', Validators.required],
        emailname: ['', Validators.required],
        emailname1: [''],
        emailname2: [''],
        usercode: [''],
        description: [''],
        fullname: ['', Validators.required],
        startdate: [currdate],
        enddate: [expdate],
        emplobj:[{}],
        suplobj:[[]],
        userlevel: [1, Validators.required],
        tenantchoosed:[{}],
        orgMlt: [[], Validators.required],
        temppass: ['', Validators.required],
        isactive: [0],
      });
      this.groupService.getAllGroupForData().subscribe((grpall: BackendResponse) => {
        this.orgsData = grpall.data.userGroup;
        // console.log(JSON.stringify(this.orgsData));
        this.formatOrgData(this.orgsData);
      });

      if (this.isEdit) {
        // {code:"3123123", label:"NANANG SUPARLAN", email:"nanang@gmail.com"},
        // {code:"3123124", label:"HETTY KOES", email:"hetty@gmail.com"},
        // {code:"3123125", label:"RINTO HASARAP", email:"rinto@gmail.com"},
        // {code:"3123126", label:"BETTY BANDEL", email:"beba@gmail.com"}
        if (this.activatedRoute.snapshot.paramMap.get('id')) {
          this.userId = this.activatedRoute.snapshot.paramMap.get('id');
          this.umService
            .retriveUsersById(this.userId)
            .subscribe(async (result: BackendResponse) => {
              console.log('Data edit user ' + JSON.stringify(result.data));
              // if (this.isOrganization) {
              this.user.username = result.data.userid;
              this.user.fullname = result.data.fullname;
              this.user.emailname = result.data.bioemailactive;
              this.user.org = result.data.id;
              this.user.password = result.data.pwd;
              this.user.active = result.data.active;
              let empObj:any = {};
              let compObj:any = {};
              if(this.user.fullname != null) {
                empObj = await this.empList.filter( employee => employee.label === this.user.fullname);
               
              }
              if(result.data.leveltenant < 2){
                this.empsupClicked = result.data.leveltenant;
                // idtenant
                // tenantchoosed":{"id":6,"tnname":"DEXA MEDICA, PT","tnstatus":1,"tntype":1,"cdidowner":1,"tnflag":1,"tnparentid":1,"cdtenant":"DXMD","created_at":"2021-12-26T16:32:10.000Z","updated_at":"2021-12-26T16:32:10.000Z"}
                compObj = await this.companyList.filter( company => company.id === result.data.idtenant);
                // console.log("HASIL company list ",compObj.length);
                if(compObj.length > 0 ){
                  this.empList=[];
                  this.empList= await this.optUnitClickedEdit({value:compObj[0]},this.umService,);
                  empObj = await this.empList.filter( employee => employee.label === this.user.fullname);
                }
                this.groupForm.patchValue({
                  username:  this.user.username,
                  fullname: this.user.fullname,
                  emailname: this.user.emailname,
                  tenantchoosed: compObj[0],
                  emplobj: empObj[0],
                  description:result.data.description,
                  usercode: result.data.usercode,
                  orgMlt: result.data.group,
                  temppass: this.user.password,
                  isactive: this.user.active,
                  emailname1:result.data.emailcc_1,
                  emailname2:result.data.emailcc_2,
                  userlevel:result.data.leveltenant
                });
              } else {

                // supList
                this.umService.retriveCompaniesByiduser(this.userId).subscribe(async (compRes:BackendResponse)=>{
                  // console.log("DATA COMPANY YANG EDIT ", compRes.data.data);
                  let datacom = compRes.data.data;
                  this.supplierselectlist=[];

                  await datacom.map(async (dataComp: any) => {
                    let obj = {company:dataComp.companyname, fullname:dataComp.vendorname, idvendor:dataComp.vendorid, status: 1, party_number:dataComp.companycode, segment1:dataComp.companycode };
                    this.supplierselectlist.push(obj);
                  });
                  console.log("DATA COMPANY YANG EDIT ", this.supplierselectlist);
                });
                this.empsupClicked = result.data.leveltenant;
                this.groupForm.patchValue({
                  username:  this.user.username,
                  fullname: this.user.fullname,
                  emailname: this.user.emailname,
                  tenantchoosed: compObj[0],
                  emplobj: null,
                  description:result.data.description,
                  usercode: result.data.usercode,
                  orgMlt: result.data.group,
                  temppass: this.user.password,
                  isactive: this.user.active,
                  emailname1:result.data.emailcc_1,
                  emailname2:result.data.emailcc_2,
                  userlevel:result.data.leveltenant
                });
              }
              
              this.groupForm.controls['fullname'].disable();
              console.log(this.user);
            });
        }
      }
    });
  }
  get f() {
    return this.groupForm.controls;
  }

  formatOrgData(data: any) {
    data.map((dt: any) => {
      let formated: any = {};
      formated.name = dt.groupname;
      formated.id = dt.id;
      this.formatedOrg.push(formated);
    });
  }

  filterOrg(event: any) {
    let filtered: any[] = [];
    let query = event.query;

    for (let i = 0; i < this.formatedOrg.length; i++) {
      let country = this.formatedOrg[i];
      if (country.name.toLowerCase().indexOf(query.toLowerCase()) > -1) {
        filtered.push(country);
      }
    }

    this.orgSuggest = filtered;
    console.log(">>>> ORG ",this.orgSuggest);
  }

  filterEmp(event: any) {
    let filtered: any[] = [];
    let query = event.query;
    for (let i = 0; i < this.empList.length; i++) {
      let country = this.empList[i];
      console.log(">>>>>>>> Employee :"+i,country);
      if (country.label.toLowerCase().indexOf(query.toLowerCase()) > -1) {
        filtered.push(country);
      }
    }
    this.empSuggest = filtered;
    console.log(">>>>>>>> Sugested :",this.empSuggest);
  }
  filterSup(event: any) {
    let filtered: any[] = [];
    let query = event.query;
    for (let i = 0; i < this.supList.length; i++) {
      let country = this.supList[i];

      if (country.label.toLowerCase().indexOf(query.toLowerCase()) > -1) {
        filtered.push(country);
      }
    }
    this.supSuggest = filtered;
  }
  
  async optUnitClickedEdit(event:any, umservice:UsermanagerService): Promise<any> {
    const promise = new Promise<any>(function (resolve, reject) {
      let employees: any = [];
      umservice.retriveEmployeeByCompanyName(event.value.tnname).subscribe(
        async (employeeResult:BackendResponse) => {
          console.log(">>>>> HASIL EMPLOYEE ", employeeResult.data);
          if(employeeResult.data.status === 200) {
            employees=[];
            let dataEmployees = employeeResult.data.data;
            await dataEmployees.map(val => {
              // console.log("Object ", val);
              let objEmpl = {
                code:val.nikemployee,
                label:val.employeename,
                email:val.email1,
                email2:val.email2
              }
              employees.push(objEmpl);
            });
            resolve(employees);
          } else {
            // this.empList=[];
            resolve(employees);
          }



        }
      );
    });
    return promise.then(function (value: any) {
      console.log(value);
      // if (value.exp == true) {
      //   router.navigate(['/auth/login']);
      //   return false;
      // }
      return value;
      // Promise returns after 1.5 second!
    });

  }
  optUnitclicked(event:any){
    // console.log("Event Employee ", event);
    let objclicked = event;
    // {"id":2,"tnname":"ANUGRAH ARGON MEDICA, PT","tnstatus":1,"tntype":1,"cdidowner":1,"tnflag":1,"tnparentid":1,"cdtenant":"AAMP","created_at":"2021-12-26T16:25:11.000Z","updated_at":"2021-12-26T16:25:11.000Z"}
    // console.log("Clicked value OU ", event.value.tnname);
    this.selectedComp = event.value.tnname;
    console.log("Event Employee ", this.selectedComp);
    // this.empsupClicked = objclicked.option.value;
    if(this.empsupClicked === 2) {
        this.operatingunitService.getDBInstance(this.selectedComp).subscribe((dbresult:BackendResponse)=>{
          console.log("DATA DB_INSTANCE ", dbresult);
          if(dbresult.status === 200) {
            let dbinstance = dbresult.data.DB_INSTANCE
            this.operatingunitService.getSupplierByDBINstance(dbinstance).subscribe((supResults:BackendResponse)=>{
              // console.log("DATA DB_INSTANCE ", supResults);
              this.supList=[];
              this.supList= supResults.data;
            });
          } else {

          }

        });

    } else {
      // console.log("INI CARI EMPLOYEEEE ");

      // this.empList = [
      //   {code:"3123123", label:"NANANG SUPARLAN", email:"nanang@gmail.com"},
      //   {code:"3123124", label:"HETTY KOES", email:"hetty@gmail.com"},
      //   {code:"3123125", label:"RINTO HASARAP", email:"rinto@gmail.com"},
      //   {code:"3123126", label:"BETTY BANDEL", email:"beba@gmail.com"}];
      
      this.umService.retriveEmployeeByCompanyName(this.selectedComp).subscribe(
        (employeeResult:BackendResponse) => {
          // console.log(">>>>> HASIL EMPLOYEE ", employeeResult.data);
          if(employeeResult.status === 200) {
            this.empList=[];
            let dataEmployees = employeeResult.data;
            dataEmployees.map(val => {
              console.log("Object ", val);
              let objEmpl = {
                code:val.nikemployee,
                label:val.employeename,
                email:val.email1,
                email2:val.email2
              }
              this.empList.push(objEmpl);
            })
          } else {
            this.empList=[];
          }



        }
      );



    }
  }

  optSplclicked(event:any){
    // console.log("Event Employee ", event);
    let objclicked = event;
    console.log("Clicked value SPL ", event.value);
    // this.empsupClicked = objclicked.option.value;
    this.selectedSpl = event.value;
    
  }
  addsuppliertables(event:any){
    console.log("Button clicked");
    if(this.selectedComp == null){
      alert('Please choose company first');
      return;
    }
    if(this.selectedSpl == null){
      alert('Please choose user');
      return;
    }
    let obj = {company:this.selectedComp, fullname:this.selectedSpl.VENDOR_NAME, idvendor:this.selectedSpl.VENDOR_ID, status: 1, party_number:this.selectedSpl.PARTY_NUMBER, segment1:this.selectedSpl.SEGMENT1 };
    this.supplierselectlist.push(obj);
  }
  delsuppliertables(payload:any){
    console.log("Button clicked ", payload);

    // this.supplierselectlist.re
    this.supplierselectlist.forEach((element,index)=>{
      if(element==payload) this.supplierselectlist.splice(index,1);
   });
    // selectspl



    // let obj = {company:this.selectedComp, fullname:this.selectedSpl.label, idvendor:this.selectedSpl.code, status: 1 };
    // this.supplierselectlist.push(obj);
  }

  empsupclicked(event:any){
    // console.log("Event Employee ", event);
    let objclicked = event;
    // console.log("Clicked value ", objclicked.option.value);
    this.empsupClicked = objclicked.option.value;
    
  }

  onSubmit() {
    this.submitted = true;
    console.log('>>>>>>>>IS NULL >>>> ' + this.leveltenant);
    // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
    if (this.groupForm.valid) {
      // event?.preventDefault;
      var groupacl = this.groupForm.get('orgobj')?.value;
      let payload = {};
      let payload2 = {};
      if (!this.isEdit) {
        if(this.empsupClicked ===1 ) {
          payload = {
            fullname: this.groupForm.get('fullname')?.value,
            usercode: this.groupForm.get('usercode')?.value,
            userid: this.groupForm.get('username')?.value,
            password: this.groupForm.get('temppass')?.value,
            group: this.groupForm.get('orgMlt')?.value,
            isactive: this.groupForm.get('isactive')?.value,
            emailname: this.groupForm.get('emailname')?.value,
            description: this.groupForm.get('description')?.value,
            startdate: this.groupForm.get('startdate')?.value,
            enddate: this.groupForm.get('enddate')?.value,
            emplobj: this.groupForm.get('emplobj')?.value,
            suplobj: this.groupForm.get('suplobj')?.value,
            orgMlt: this.groupForm.get('orgMlt')?.value,
            userlevel:this.groupForm.get('userlevel')?.value,
            temppass: this.groupForm.get('temppass')?.value,
            tenantchoosed: this.groupForm.get('tenantchoosed')?.value,
            emailname1:this.groupForm.get('emailname1')?.value,
            emailname2:this.groupForm.get('emailname2')?.value,
          };
          // payload2 = {
          //   email: this.groupForm.value['emailname'],
          // };
  
          console.log('>>>>>>>> payload ' + JSON.stringify(payload));
          this.umService.insertByAdmin(payload).subscribe(
            (result: BackendResponse) => {
              console.log("Result on save ", result)
              if (result.status === 200) {
                      this.location.back();
              }
            },
            (err) => {
              console.log(err);
              this.showTopCenterErr(err.error.data);
            }
          );
        } else {
          // alert('Under construction');
          payload = {
            fullname: this.groupForm.get('fullname')?.value,
            usercode: this.groupForm.get('usercode')?.value,
            userid: this.groupForm.get('username')?.value,
            password: this.groupForm.get('temppass')?.value,
            group: this.groupForm.get('orgMlt')?.value,
            isactive: this.groupForm.get('isactive')?.value,
            emailname: this.groupForm.get('emailname')?.value,
            description: this.groupForm.get('description')?.value,
            startdate: this.groupForm.get('startdate')?.value,
            enddate: this.groupForm.get('enddate')?.value,
            emplobj: this.groupForm.get('emplobj')?.value,
            suplobj: this.supplierselectlist,
            orgMlt: this.groupForm.get('orgMlt')?.value,
            userlevel:this.groupForm.get('userlevel')?.value,
            temppass: this.groupForm.get('temppass')?.value,
            // tenantchoosed: this.groupForm.get('tenantchoosed')?.value,
            tenantchoosed: {id:0, label:"Supplier"},
            emailname1:this.groupForm.get('emailname1')?.value,
            emailname2:this.groupForm.get('emailname2')?.value,
          };

          console.log(">>>> Payload ", payload);
          this.umService.insertByAdmin(payload).subscribe(
            (result: BackendResponse) => {
              console.log("Result on save ", result)
              if (result.status === 200) {
                      this.location.back();
              }
            },
            (err) => {
              console.log(err);
              this.showTopCenterErr(err.error.data);
            }
          );







          // this.location.back();
        }
      } else {

        if(this.empsupClicked ===1 ) {
          payload = {
            id:this.userId,
            fullname: this.groupForm.get('fullname')?.value,
            usercode: this.groupForm.get('usercode')?.value,
            userid: this.groupForm.get('username')?.value,
            password: this.groupForm.get('temppass')?.value,
            group: this.groupForm.get('orgMlt')?.value,
            isactive: this.groupForm.get('isactive')?.value,
            emailname: this.groupForm.get('emailname')?.value,
            description: this.groupForm.get('description')?.value,
            startdate: this.groupForm.get('startdate')?.value,
            enddate: this.groupForm.get('enddate')?.value,
            emplobj: this.groupForm.get('emplobj')?.value,
            suplobj: this.groupForm.get('suplobj')?.value,
            orgMlt: this.groupForm.get('orgMlt')?.value,
            userlevel:this.groupForm.get('userlevel')?.value,
            temppass: this.groupForm.get('temppass')?.value,
            tenantchoosed: this.groupForm.get('tenantchoosed')?.value,
            emailname1:this.groupForm.get('emailname1')?.value,
            emailname2:this.groupForm.get('emailname2')?.value,
          };
          console.log('>>>>>>>> payload ' + JSON.stringify(payload));
          this.umService
            .updatebyAdmin(payload)
            .subscribe((result: BackendResponse) => {
              console.log(">>>>>>>> return "+JSON.stringify(result));
              if (result.status === 200) {
                this.location.back();
              }
            });
        } else {
          alert('Under construction');
          this.location.back();
        }


        
         }






    }

    console.log(this.groupForm.valid);
  }

  employenameSelected(event:any){
    console.log("Employee selected ", event);
    this.groupForm.patchValue({fullname: event.label});
    this.groupForm.patchValue({emailname: event.email});
    this.groupForm.patchValue({usercode: event.code});
  }

  showTopCenterErr(message: string) {
    this.messageService.add({
      severity: 'error',
      summary: 'Error',
      detail: message,
    });
  }
}
