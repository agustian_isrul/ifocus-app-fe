import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-choosecompany',
  templateUrl: './choosecompany.component.html',
  styleUrls: ['./choosecompany.component.scss']
})
export class ChoosecompanyComponent implements OnInit {
  companies: any[];
  selectedCompany: any;
  constructor() { }

  ngOnInit(): void {
  }

}
