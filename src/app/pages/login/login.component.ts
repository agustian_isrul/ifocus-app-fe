import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionStorageService } from 'angular-web-storage';
import { BackendService } from 'src/app/services/backend.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { RecaptchaErrorParameters } from 'ng-recaptcha';
import { Location } from '@angular/common';
import { TenantmanagerService } from 'src/app/services/root/tenantmanager.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  companies: any[] = [];
  selectedCompany: any;
  blockedDocument: boolean = false;
  blockedCompany:boolean = true;
  //  userid = "";
  captchaSiteKey: string = '6LcvgrQcAAAAAB_tLF7BUtS1p2xY2sI8tvNxJW94';
  secret = '';
  errorMsg = '';
  isProcess: boolean = false;
  userForm!: FormGroup;
  compForm!: FormGroup;
  submitted = false;
  constructor(
    private messageService: MessageService,
    private sessionStorage: SessionStorageService,
    private route: Router,
    private formBuilder: FormBuilder,
    private backend: BackendService,
    private authservice: AuthService,
   private tenantService: TenantmanagerService
  ) {}

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      userid: ['', Validators.required],
      secret: ['', Validators.required],
      // recaptcha: ['', Validators.required],
    });
    this.compForm = this.formBuilder.group({
      selectedCompany: ['', Validators.required],
    });
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.userForm.controls;
  }

  get comp() {
    return this.compForm.controls;
  }


  get userFormControl() {
    return this.userForm.controls;
  }
  get compFormControl(){
    return this.compForm.controls;
  }

  public resolved(captchaResponse: string): void {
    console.log(`Resolved captcha with response: ${captchaResponse}`);
  }

  public onError(errorDetails: RecaptchaErrorParameters): void {
    console.log(`reCAPTCHA error encountered; details:`, errorDetails);
  }
  onSubmitComp(){
    console.log(">>>>>>>>>>>>>>>>>>>>>>>>> LOGIN DASHBOARD <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    // signviacompanysc
    this.submitted = true;
    if (this.userForm.valid) {
      console.log(">>>>>>>>>>>>>>>>>>>> Login by Company ");

      console.log(">>> ",this.userForm.value);
      console.log(">>> ",this.compForm.value);
      // selectedCompany: {id: 2, label: 'ANUGRAH ARGON MEDICA, PT'}
      this.isProcess = true;
      let tenantChoosedID = this.compForm.value['selectedCompany']
      this.backend
        .post(
          'adm/auth/signviacompanysc',
          {
            credential: this.userForm.value['userid'],
            secret: this.userForm.value['secret'],
            appname: 'ifocus',
            tnid:tenantChoosedID.id
          },
          false
        )
        .subscribe((data:any) => {
          console.log(">>>>>>> Ang Dari Server "+JSON.stringify(data));
          if (data.status === 200) {
            console.log("INI PAKE ROUTE Company ", JSON.stringify(data));
            this.sessionStorage.clear();
            const authToken = data.data;
            this.sessionStorage.set('accesstoken', authToken);
            this.authservice.setAuthStatus(true);
            this.authservice.setToken(authToken);
            this.route.navigate(['mgm/home/notification/']);
            this.isProcess = false;
            
          } else {
            this.sessionStorage.clear();
            this.showTopCenterErr(
              `${data.data ? data.data : 'Invalid username or password'}`
            );
            this.authservice.setAuthStatus(false);
            this.isProcess = false;
          }
        },
        (error) => {
          this.showTopCenterErr('Invalid user and password');
          this.isProcess = false;
          this.authservice.setAuthStatus(false);
        });



    }
  }


  onSubmit() {
    this.submitted = true;

    if (this.userForm.valid) {
      this.isProcess = true;
      this.backend
        .post(
          'adm/auth/signviaadminsc',
          {
            credential: this.userForm.value['userid'],
            secret: this.userForm.value['secret'],
            appname: 'ifocus',
          },
          false
        )
        .subscribe(
          (data:any) => {
            // console.log(">>>>>>> Ang Dari Server "+JSON.stringify(data));
            if (data.status === 200) {
              console.log("NGGA PAKE ROUTE ", JSON.stringify(data));
              const authToken = data.data;
              this.sessionStorage.set('accesstoken', authToken);
              this.authservice.setAuthStatus(true);
              this.authservice.setToken(authToken);

              if(data.tokenId === "XXXXXX") {
                // NGGA PAKE ROUTE  {"status":200,"data":{"id":42,"credential":"AFNIMAR","password":"manage","iat":1642662312},"tokenId":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NDIsImNyZWRlbnRpYWwiOiJBRk5JTUFSIiwicGFzc3dvcmQiOiJtYW5hZ2UiLCJpYXQiOjE2NDI2NjIzMTJ9.4a6GvlueGKQ_fMtR4xIKN3MK99AbHjcGehD4pBeLIp0","exp":false} 
                //  this.authservice.whoAmi().subscribe((data:BackendResponse) => {
                  // console.log("NGGA PAKE ROUTE ", JSON.stringify(data));
                //   let idUser= data.data.id;
                //   this.isProcess = false;
                //  });
                this.tenantService.retriveTenantByNamein(data.companylist).subscribe(async (compres:BackendResponse)=>{
                  // console.log("TENANTNYA ", JSON.stringify(compres));
                  // TENANTNYA  {"status":200,"data":[{"id":2,"tnname":"ANUGRAH ARGON MEDICA, PT","tnstatus":1,"tntype":1,"cdidowner":1,"tnflag":1,"tnparentid":1,"cdtenant":"AAMP","created_at":"2021-12-26T16:25:11.000Z","updated_at":"2021-12-26T16:25:11.000Z"},{"id":7,"tnname":"DJEMBATAN DUA, PT","tnstatus":1,"tntype":1,"cdidowner":1,"tnflag":1,"tnparentid":1,"cdtenant":"DDPT","created_at":"2021-12-26T16:32:37.000Z","updated_at":"2021-12-26T16:32:37.000Z"}]}

                  // compres.data
                  this.companies = [];
                  await compres.data.map((jsobj) => {
                    let object = {id:jsobj.id, label:jsobj.tnname}
                    this.companies.push(object);
                  });



                })


                 this.isProcess = false;
                this.blockedCompany=false;
                
                

              } else {
                // this.sessionStorage.set('accesstoken', authToken);
                // this.authservice.setAuthStatus(true);
                // this.authservice.setToken(authToken);
                this.route.navigate(['mgm/home']);
                this.isProcess = false;
              }


              // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
              //   console.log('>>>>>>> ' + JSON.stringify(data));
              //   if ((data.status = 200)) {
              //     let objecInfo = data.data;
              //     let lvlTn = parseInt(objecInfo.leveltenant);
              //     console.log('>> LVL TENANT >> ' + lvlTn);
              //     //   if(objecInfo.leveltenant < 1) {
              // this.route.navigate(['/' + tokenID]);
              //     //   } else {
              // this.route.navigate(['/mgm/admin']);
              //     //   }
              //   }
              // });
              // this.route.navigate(['mgm/home/notification/']);
              // this.isProcess = false;
            } else {
              this.sessionStorage.clear();
              this.showTopCenterErr(
                `${data.data ? data.data : 'Invalid username or password'}`
              );
              this.authservice.setAuthStatus(false);
              this.isProcess = false;
            }
          },
          (error) => {
            this.showTopCenterErr('Invalid user and password');
            this.isProcess = false;
            this.authservice.setAuthStatus(false);
          }
        );
    }

    //  this.blockDocument();
  }
  showTopCenterErr(message: string) {
    this.messageService.add({
      severity: 'error',
      summary: 'Error',
      detail: message,
    });
  }
  cancelCompany(){
    this.sessionStorage.clear();
    this.authservice.setAuthStatus(false);
    this.authservice.setToken(null);
    this.blockedCompany=true;
    this.companies=[];
  }
  blockDocument() {
    this.blockedDocument = true;
    //  setTimeout(() => {
    //      this.blockedDocument = false;
    //      this.showTopCenterErr("Invalid user and password!")
    //  }, 3000);
  }
}
