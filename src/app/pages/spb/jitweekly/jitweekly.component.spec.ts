import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JitweeklyComponent } from './jitweekly.component';

describe('JitweeklyComponent', () => {
  let component: JitweeklyComponent;
  let fixture: ComponentFixture<JitweeklyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JitweeklyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JitweeklyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
