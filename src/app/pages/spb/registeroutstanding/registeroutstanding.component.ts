import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/services/spb/customer.service';
import { Capa } from 'src/app/services/spb/customer';
import { MenuItem, MessageService } from 'primeng/api';

@Component({
  selector: 'app-registeroutstanding',
  templateUrl: './registeroutstanding.component.html',
  styleUrls: ['./registeroutstanding.component.scss']
})
export class RegisteroutstandingComponent implements OnInit {
  capa: Capa[];
  first = 0;
  rows = 10;

  statuses: any[];
  loading: boolean = true;

  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching:boolean = false;

  constructor(
    private customerService: CustomerService,
    public messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/mgm/home'};
    this.breadcrumbs = [
      { label: 'Register Outstanding' }
    ];
    
    this.customerService.getCustomersSmall().then(capa => this.capa = capa);
    this.loading = false;

    this.capa.forEach(
      capa => (capa.date = new Date(capa.date))
    );
    this.statuses = [
      {label: 'Open', value: 'open'},
      {label: 'Close', value: 'close'}
    ]
    }

    next() {
        this.first = this.first + this.rows;
    }

    prev() {
        this.first = this.first - this.rows;
    }

    reset() {
        this.first = 0;
    }

    isLastPage(): boolean {
        return this.capa ? this.first === (this.capa.length - this.rows): true;
    }

    isFirstPage(): boolean {
        return this.capa ? this.first === 0 : true;
    }
  }
