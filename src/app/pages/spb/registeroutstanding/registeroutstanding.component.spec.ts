import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisteroutstandingComponent } from './registeroutstanding.component';

describe('RegisteroutstandingComponent', () => {
  let component: RegisteroutstandingComponent;
  let fixture: ComponentFixture<RegisteroutstandingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisteroutstandingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisteroutstandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
