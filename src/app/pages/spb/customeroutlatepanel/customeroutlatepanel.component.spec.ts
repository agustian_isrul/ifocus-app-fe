import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomeroutlatepanelComponent } from './customeroutlatepanel.component';

describe('CustomeroutlatepanelComponent', () => {
  let component: CustomeroutlatepanelComponent;
  let fixture: ComponentFixture<CustomeroutlatepanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomeroutlatepanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomeroutlatepanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
