import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { MenuItem, Message, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { NotificationlistService } from 'src/app/services/spb/notificationlist.service';
import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';
import { CustomerPanelService } from 'src/app/services/spb/customer-panel.service';
interface Type {
  name: string
}

interface Dropdown {
  name: string,
  value: string
}

interface CustomerOutlet {
  outletId: string,
  outletName: string,
  channel: string,
  areaLine: string,
  startDate: string
  endDate: string
}


@Component({
  selector: 'app-customeroutlatepanel',
  templateUrl: './customeroutlatepanel.component.html',
  styleUrls: ['./customeroutlatepanel.component.scss']
})
export class CustomeroutlatepanelComponent implements OnInit {

  
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching: boolean = false;
  tokenID: string = '';
  display = false;

  typeselected: any = [];
  typevalue: Type[];
  areaLineList: Dropdown[];
  chanelList: Dropdown[];
  dialogCustomer: boolean;
  customerOutletList : CustomerOutlet[];

  selectAllOutlet: boolean = false;
  selectedCustomersOutlet: any[];

  //==== modal Start ====
  areaLineSelected:any = {};
  channelselected:any = {};
  startdate: string = '';
  endingdate: string = '';
  inputCustomerNameModal:'';
  customerList: any[] = [];
  selectAll: boolean = false;
  selectedCustomers: any[];
  // ==== modal End ====
 
  constructor(private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private notifikasiListService: NotificationlistService,
    private aclMenuService : AclmenucheckerService,
    private customerPanelService : CustomerPanelService,
    private datePipe: DatePipe,
    private router: Router) { }

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Customer Outlet panel' }];

    this.areaLineList = [
      {name: 'Dki Jakarta', value: 'jakarta'},
      {name: 'Jawa Barat', value: 'jawabarat'},
      {name: 'Jawa Tengah', value: 'jawatengah'},
      {name: 'Kalimantan', value: 'kalimanatn'}
    ];
    this.chanelList = [
      {name: 'Chanel 1', value: 'chanel1'},
      {name: 'chanel 2', value: 'chanel2'},
      {name: 'Chanel 3', value: 'chanel3'},
      {name: 'Chanel 4', value: 'chanel4'}
    ];
   
    this.refreshingUser();
  }

  refreshingUser() {
    this.isFetching = true;
    this.customerOutletList = [];
  }

  openDialog(){
    this.dialogCustomer = true;
  }
  hideDialog(){
    this.dialogCustomer = false;
  }

  btnDeleteData (){
   
    if(this.selectedCustomersOutlet != undefined){
        this.deleteConfirmation();
    }else{
      this.messageService.add({severity:'error', summary:'Warning', detail:'Harap Pilih Data Yang ingin Di Hapus !'});
    }
  }

  deleteProcess (){
    const dataTemp =[];
    this.customerOutletList.forEach(dataOutlet => {
      let status = true;
      this.selectedCustomersOutlet.forEach(dataHapus => {
          if(dataOutlet.outletId == dataHapus.outletId){
            status =false;
          }
      });

      if(status){
        let objtmp = dataOutlet;
        dataTemp.push(objtmp);
      }
    });

    this.customerOutletList= [];
    this.customerOutletList=dataTemp;
    this.display = false;
    this.messageService.add({severity:'error', summary:'Warning', detail:'Data has been Deleted'});
  }

  saveProcess (){
    const dataTemp =[];
    this.customerOutletList.forEach(dataOutlet => {
      let status = true;
      this.selectedCustomersOutlet.forEach(dataHapus => {
          if(dataOutlet.outletId == dataHapus.outletId){
            status =false;
          }
      });

      if(status){
        let objtmp = dataOutlet;
        dataTemp.push(objtmp);
      }
    });

    this.customerOutletList= [];
    this.customerOutletList=dataTemp;
    this.display = false;
    this.messageService.add({severity:'success', summary:'Success', detail:'Data has been Saved'});
  }

  btnSaveData (){
   
    if(this.selectedCustomersOutlet != undefined){
      this.saveProcess();
    }else{
      this.messageService.add({severity:'error', summary:'Warning', detail:'Harap Pilih Data Yang ingin Di Hapus !'});
    }
  }

  deleteConfirmation() {
    this.display = true;
  }

  btnSearchModal(){
    console.log(this.startdate);
    this.customerList = [];
    this.customerPanelService.getCustomerNyCompanyIdAreaCustomerName().subscribe((orgall: BackendResponse) => {
    
      this.customerList = orgall.data;
      
      if (this.customerList.length < 1) {
        let objtmp = {
          outletId: 'No records',
          outletName: 'No records',
          address: 'No records'
          
        };
        this.customerList = [];
        this.customerList.push(objtmp);
      }

      this.isFetching = false;
    });
  }

  btnAddDataModal(){
    console.log("testing add data modal");
    console.log(this.selectedCustomers);

    this.selectedCustomers.forEach(data => {
      let objtmp = {
        outletId: data.outletId,
        outletName: data.outletName,
        channel: this.channelselected.name,
        areaLine: this.areaLineSelected.name,
        startDate:  this.datePipe.transform(this.startdate, 'yyyy-MM-dd'),
        endDate: this.datePipe.transform(this.endingdate, 'yyyy-MM-dd')
      };
      this.customerOutletList.push(objtmp);
    });

    this.hideDialog();
  }

  onSelectionChangeOutlet(value = []) {
    this.selectAllOutlet = value.length === this.customerOutletList.length;
    this.selectedCustomersOutlet = value;
  }

  onSelectAllChangeOutlet(event) {
      const checked = event.checked;    
  }

  onSelectionChange(value = []) {
    this.selectAll = value.length === this.customerList.length;
    this.selectedCustomers = value;
  }

  onSelectAllChange(event) {
      const checked = event.checked;    
  }


}
