import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocsetupComponent } from './docsetup.component';

describe('DocsetupComponent', () => {
  let component: DocsetupComponent;
  let fixture: ComponentFixture<DocsetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocsetupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocsetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
