import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { CustomerService } from 'src/app/services/spb/customer.service';

interface Supplier {
  name : string,
  value : string
}

interface Tahun {
  nomor : number;
}

interface Jit {
  name : string,
  value : string
}

@Component({
  selector: 'app-spereport',
  templateUrl: './spereport.component.html',
  styleUrls: ['./spereport.component.scss']
})
export class SpereportComponent implements OnInit {
  
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching:boolean = true;

  supplier: Supplier[];
  selectedSupplier: Supplier;

  jit: Jit[];
  selectedJit: Jit;

  tahun: Tahun[];
  selectedTahun: Tahun;

  pencet:boolean=false;

  constructor(
    private customerService: CustomerService,
    public messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/mgm/home'};
  this.breadcrumbs = [
    { label: 'SPE Report' }
  ];

  this.supplier = [
    {name: 'ALLFORM INDONESIA, PT', value: 'ai'},
    {name: 'ALTINEX, PT', value: 'al'},
    {name: 'ANCOL TERANG METAL PRINTING INDUSTRI, PT', value:'atmpi'},
    {name: 'ANINDOJAYA SWAKARYA, PT', value: 'as'},
    {name: 'ARISTROCAT CIPTA MANDIRI, PT', value: 'acm'}
];

this.jit = [
  {name: 'Non JIT', value: 'non'},
  {name: 'JIT', value: 'jit'},
];

this.tahun = [
  {nomor: 2009},
  {nomor: 2010},
  {nomor: 2011},
  {nomor: 2012},
  {nomor: 2013}
];
  }
  tooglePencet(){
    this.pencet=!this.pencet
  }
  // printComponen() {
  //   let printContents = document.getElementById().innerHTML;
  //   let originalContents = document.body.innerHTML;

  //   document.body.innerHTML = printContents;

  //   window.print();

  //   document.body.innerHTML = originalContents;
  // }

}
