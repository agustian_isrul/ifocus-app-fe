import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpereportComponent } from './spereport.component';

describe('SpereportComponent', () => {
  let component: SpereportComponent;
  let fixture: ComponentFixture<SpereportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpereportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpereportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
