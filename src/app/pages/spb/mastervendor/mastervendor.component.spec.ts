import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MastervendorComponent } from './mastervendor.component';

describe('MastervendorComponent', () => {
  let component: MastervendorComponent;
  let fixture: ComponentFixture<MastervendorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MastervendorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MastervendorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
