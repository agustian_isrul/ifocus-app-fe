import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MastervendordetailComponent } from './mastervendordetail.component';

describe('MastervendordetailComponent', () => {
  let component: MastervendordetailComponent;
  let fixture: ComponentFixture<MastervendordetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MastervendordetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MastervendordetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
