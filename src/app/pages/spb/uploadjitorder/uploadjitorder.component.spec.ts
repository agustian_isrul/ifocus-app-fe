import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadjitorderComponent } from './uploadjitorder.component';

describe('UploadjitorderComponent', () => {
  let component: UploadjitorderComponent;
  let fixture: ComponentFixture<UploadjitorderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadjitorderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadjitorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
