import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';

interface Type {
  name: string
}

@Component({
  selector: 'app-uploadjitorder',
  templateUrl: './uploadjitorder.component.html',
  styleUrls: ['./uploadjitorder.component.scss']
})
export class UploadjitorderComponent implements OnInit {
  uploadedfiles: any[] = [];

  typevalue: Type[];
  type: any = [];
  typeselected: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;

  addbtn = { route: 'detail', label: 'Create JIT' };

  constructor(private messageService: MessageService) {
    this.typevalue = [
      {name: 'Daily'},
      {name: 'Weekly',},
  ];
  }

  onUpload(event) {
      for(let file of event.files) {
          this.uploadedfiles.push(file);
      }

      this.messageService.add({severity: 'info', summary: 'File Uploaded', detail: ''});
  }

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Upload JIT Order' }];
  }

  // createJit(){
  //   let payload = {
  //     type: this.typeSelected,
  //   };
  //   this.notifikasiListService.getByModulStatus(payload).subscribe((orgall: BackendResponse) => {
  //     // console.log('>>>>>>> ' + JSON.stringify(orgall));
  //     this.notiflist = orgall.data;
  //     if (this.notiflist.length < 1) {
  //       let objtmp = {
  //         module: 'No records',
  //         subject: 'No records',
  //         receive_date: 'No records',
  //         status: 'No records',
  //       };
  //       this.notiflist = [];
  //       this.notiflist.push(objtmp);
  //     }
  //     this.isFetching = false;
  //   });
    
  // } 
  // }

  onChange(event) {
    console.log('event :' + event);
    console.log(event.value.name);

    if(event.value.name == "Daily"){
      console.log("masuk");
    } else if(event.value.name == "Weekly"){
      console.log("masuk week");
    }
  }

}
