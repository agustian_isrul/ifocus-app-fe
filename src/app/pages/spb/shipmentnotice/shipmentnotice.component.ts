import { Component, OnInit } from '@angular/core';
import { MenuItem, PrimeNGConfig, TreeNode } from 'primeng/api';
import { Customerr, Product, Representative } from 'src/app/services/spb/customer';
import { CustomerService } from 'src/app/services/spb/customer.service';

@Component({
  selector: 'app-shipmentnotice',
  templateUrl: './shipmentnotice.component.html',
  styleUrls: ['./shipmentnotice.component.scss'],
})
export class ShipmentnoticeComponent implements OnInit {
  customers: Customerr[];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;

  statuses: any[];
  
  rowGroupMetadata: any;

  statusess: any[];

  constructor(
    private customerService: CustomerService,
    private primengConfig: PrimeNGConfig
    ) { }

  ngOnInit(): void {

    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home'};
    this.breadcrumbs = [
      { label: 'Shipment Notice'}
    ]
    this.statuses = [
      {label: 'Draft', value: 'draft'},
      {label: 'Submitted', value: 'submitted'},
      {label: 'Close', value: 'close'}
  ]


  this.customerService.getCustomersMediumm().then(data => {
    this.customers = data;
    this.updateRowGroupMetaData();
});
}
onSort() {
  this.updateRowGroupMetaData();
}

updateRowGroupMetaData() {
  this.rowGroupMetadata = {};

  if (this.customers) {
      for (let i = 0; i < this.customers.length; i++) {
          let rowData = this.customers[i];
          let representativeName = rowData.representative.name;
          
          if (i == 0) {
              this.rowGroupMetadata[representativeName] = { index: 0, size: 1 };
          }
          else {
              let previousRowData = this.customers[i - 1];
              let previousRowGroup = previousRowData.representative.name;
              if (representativeName === previousRowGroup)
                  this.rowGroupMetadata[representativeName].size++;
              else
                  this.rowGroupMetadata[representativeName] = { index: i, size: 1 };
          }
      }
  }
}


}

