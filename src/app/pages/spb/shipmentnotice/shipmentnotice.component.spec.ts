import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShipmentnoticeComponent } from './shipmentnotice.component';

describe('ShipmentnoticeComponent', () => {
  let component: ShipmentnoticeComponent;
  let fixture: ComponentFixture<ShipmentnoticeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShipmentnoticeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipmentnoticeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
