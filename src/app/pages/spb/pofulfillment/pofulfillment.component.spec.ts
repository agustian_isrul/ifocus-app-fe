import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PofulfillmentComponent } from './pofulfillment.component';

describe('PofulfillmentComponent', () => {
  let component: PofulfillmentComponent;
  let fixture: ComponentFixture<PofulfillmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PofulfillmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PofulfillmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
