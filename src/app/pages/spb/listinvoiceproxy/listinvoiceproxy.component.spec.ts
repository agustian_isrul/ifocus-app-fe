import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListinvoiceproxyComponent } from './listinvoiceproxy.component';

describe('ListinvoiceproxyComponent', () => {
  let component: ListinvoiceproxyComponent;
  let fixture: ComponentFixture<ListinvoiceproxyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListinvoiceproxyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListinvoiceproxyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
