import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicenonpoComponent } from './invoicenonpo.component';

describe('InvoicenonpoComponent', () => {
  let component: InvoicenonpoComponent;
  let fixture: ComponentFixture<InvoicenonpoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvoicenonpoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicenonpoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
