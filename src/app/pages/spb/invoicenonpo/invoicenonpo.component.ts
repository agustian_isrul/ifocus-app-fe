import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Capa } from 'src/app/services/spb/customer';
import { CustomerService } from 'src/app/services/spb/customer.service';


interface Company {
  name: string
}

@Component({
  selector: 'app-invoicenonpo',
  templateUrl: './invoicenonpo.component.html',
  styleUrls: ['./invoicenonpo.component.scss']
})
export class InvoicenonpoComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  company: Company[];
  value: Date;
  value1: number;
  items: any[];
  capa: Capa[];

  selectedCompany: Company
  selectedItem: any;
  filteredItems: any[];
  first = 0;
  rows = 10;
  loading: boolean = true;

  constructor(
    private customerService : CustomerService
  ) { }

  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/mgm/home'};
    this.breadcrumbs = [
      { label: 'Invoice Proxy non-PO'}
    ];
    this.company = [
      {name: 'Dexa'},
      {name: 'Something'},
      {name: 'I kinda'},
      {name: 'My foot'},
      {name: 'Sunshine'}
  ];
  this.items = [];
        for (let i = 0; i < 10000; i++) {
            this.items.push({label: 'Item ' + i, value: 'Item ' + i});
        }
    this.customerService.getCustomersSmall().then(capa => this.capa = capa);
    this.loading = false;
    
    this.capa.forEach(
      capa => (capa.date = new Date(capa.date))
    );
  }
  filterItems(event) {
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered : any[] = [];
    let query = event.query;

    for(let i = 0; i < this.items.length; i++) {
        let item = this.items[i];
        if (item.label.toLowerCase().indexOf(query.toLowerCase()) == 0) {
            filtered.push(item);
        }
    }

    this.filteredItems = filtered;
  }

}
