import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewnotificationdetailComponent } from './viewnotificationdetail.component';

describe('ViewnotificationdetailComponent', () => {
  let component: ViewnotificationdetailComponent;
  let fixture: ComponentFixture<ViewnotificationdetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewnotificationdetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewnotificationdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
