import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/services/spb/customer.service';
import { Customer, Representative, IDepartment, Capa } from 'src/app/services/spb/customer';

@Component({
  selector: 'app-viewnotificationdetail',
  templateUrl: './viewnotificationdetail.component.html',
  styleUrls: ['./viewnotificationdetail.component.scss']
})
export class ViewnotificationdetailComponent implements OnInit {

  capa: Capa[];
  customers: Customer[];
  first = 0;
  rows = 10;
  checked: boolean = false;
  checked1: boolean = false;
  isDisplay = false;
  loading: boolean = true;

  toggleDisplayy(){
    this.checked = !this.checked
  }
  toggleTabel(){
    this.checked1 = !this.checked1
  }

  constructor(private customerService: CustomerService) { }

  ngOnInit(): void {
    this.customerService.getCustomersSmall().then(capa => {
      this.capa = capa;
      this.loading = false;
    });
  }

  next() {
      this.first = this.first + this.rows;
  }

  prev() {
      this.first = this.first - this.rows;
  }

  reset() {
      this.first = 0;
  }

  isLastPage(): boolean {
      return this.customers ? this.first === (this.customers.length - this.rows): true;
  }

  isFirstPage(): boolean {
      return this.customers ? this.first === 0 : true;
  }

}
