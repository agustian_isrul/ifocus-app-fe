import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { GroupServiceService } from 'src/app/services/root/group-service.service';
import { SpecquoteService } from 'src/app/services/spb/specquote.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-specquotedetail',
  templateUrl: './specquotedetail.component.html',
  styleUrls: ['./specquotedetail.component.scss']
})
export class SpecquotedetailComponent implements OnInit {
  extraInfo: any = {};
  isEdit: boolean = false;
  userId: any = null;
  stateOptions: any[] = [];
  stateOptionsEdit: any[] = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo: any = {};
  selectedApps: any = [];
  tokenID: string = '';
  groupForm!: FormGroup;
  submitted = false;
  orgsData: any[] = [];
  appInfoActive: any = {};
  orgSuggest: any = {};
  user: any = {};
  formatedOrg: any[] = [];
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private specQuoteService: SpecquoteService,
    private authservice: AuthService,
    private groupService: GroupServiceService,
    private messageService: MessageService,
    private location: Location,
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    let checkurl = this.extraInfo.indexOf('%23') !== -1 ? true : false;
    console.log('>>>>>>>>>>> ' + this.extraInfo);
    console.log(checkurl);
    if (checkurl) this.isEdit = true;
  }

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      {
        label: 'Item JIT',
        command: (event) => {
          this.location.back();
        },
        url: '',
      },
      { label: this.isEdit ? 'Edit data' : 'Add data' },
    ];
    this.stateOptions = [
      { label: '- SELECT -', value: 1 },
      // { label: 'Activision link', value: 0 },
    ];
    this.stateOptionsEdit = [
      { label: '- SELECT -', value: 1 },
      // { label: 'Deactive', value: 0 },
    ];
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      this.groupForm = this.formBuilder.group({
        itemnumber: ['', Validators.required],
        description: ['', Validators.required],
        color: ['', Validators.required],
        size: ['', Validators.required],
        remark_design: [[], Validators.required],
      });
      this.groupService.getAllGroupForData().subscribe((grpall: BackendResponse) => {
        this.orgsData = grpall.data.userGroup;
        // console.log(JSON.stringify(this.orgsData));
        this.formatOrgData(this.orgsData);
      });

      if (this.isEdit) {
        if (this.activatedRoute.snapshot.paramMap.get('id')) {
          this.userId = this.activatedRoute.snapshot.paramMap.get('id');
          this.specQuoteService
            .retrivespecquoteById(this.userId)
            .subscribe(async (result: BackendResponse) => {
              console.log('Data edit user ' + JSON.stringify(result.data));
              // if (this.isremark_design) {
              this.user.itemnumber = result.data.itemnumberid;
              this.user.size = result.data.size;
              this.user.color = result.data.color;
              this.user.description = result.data.description;
              this.user.remark_design = result.data.remark_design;
               this.groupForm.patchValue({
                itemnumber:  this.user.itemnumber,
                size: this.user.size,
                color: this.user.color,
                remark_design: this.user.remark_design,
                description: this.user.description,
              });
              this.groupForm.controls['itemnumber'].disable();
              console.log(this.user);
            });
        }
      }
    });
  }
  get f() {
    return this.groupForm.controls;
  }
  formatOrgData(data: any) {
    data.map((dt: any) => {
      let formated: any = {};
      formated.name = dt.groupname;
      formated.id = dt.id;
      this.formatedOrg.push(formated);
    });
  }

  filterOrg(event: any) {
    let filtered: any[] = [];
    let query = event.query;

    for (let i = 0; i < this.formatedOrg.length; i++) {
      let country = this.formatedOrg[i];
      if (country.name.toLowerCase().indexOf(query.toLowerCase()) > -1) {
        filtered.push(country);
      }
    }

    this.orgSuggest = filtered;
  }

  onSubmit() {
    this.submitted = true;
    console.log('>>>>>>>>IS NULL >>>> ' + this.leveltenant);
    // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
    if (this.groupForm.valid) {
      // event?.preventDefault;
      let payload = {};

      if (!this.isEdit) {
        payload = {
          itemnumber:  this.groupForm.get('itemnumber')?.value,
          size: this.groupForm.get('size')?.value,
          color: this.groupForm.get('color')?.value,
          remark_design: this.groupForm.get('remark_design')?.value,
          description: this.groupForm.get('description')?.value,
        };
        console.log('>>>>>>>> payload ' + JSON.stringify(payload));
        this.specQuoteService.insertspecquote(payload).subscribe(
          (result: BackendResponse) => {
            if (result.status === 200) {
              this.location.back();
            } (err) => {
              console.log(err);
              this.showTopCenterErr(err.error.data);
            }
          },
          (err) => {
            console.log(err);
            this.showTopCenterErr(err.error.data);
          }
        );
      } else {
        payload = {
          itemnumber:  this.userId,
          size: this.groupForm.get('size')?.value,
          color: this.groupForm.get('color')?.value,
          remark_design: this.groupForm.get('remark_design')?.value,
          description: this.groupForm.get('description')?.value,
        };
        console.log('>>>>>>>> payload ' + JSON.stringify(payload));
        this.specQuoteService
          .updatespecquote(payload)
          .subscribe((result: BackendResponse) => {
            // console.log(">>>>>>>> return "+JSON.stringify(result));
            if (result.status === 200) {
              this.location.back();
            }
          });
      }
    }

    console.log(this.groupForm.valid);
  }
  showTopCenterErr(message: string) {
    this.messageService.add({
      severity: 'error',
      summary: 'Error',
      detail: message,
    });
  }
}
