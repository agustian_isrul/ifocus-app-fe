import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecquotedetailComponent } from './specquotedetail.component';

describe('SpecquotedetailComponent', () => {
  let component: SpecquotedetailComponent;
  let fixture: ComponentFixture<SpecquotedetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecquotedetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecquotedetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
