import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, Message, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { SpecquoteService } from 'src/app/services/spb/specquote.service';
import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';

@Component({
  selector: 'app-specquote',
  templateUrl: './specquote.component.html',
  styleUrls: ['./specquote.component.scss']
})
export class SpecquoteComponent implements OnInit {
  display = false;
  scrollheight:any ="400px"
  viewDisplay = false;
  viewApprove = false;
  selectedItemJit: any = {};
  isGroup = false;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  itemjitlist: any[] = [];
  isFetching: boolean = false;
  itemJitInfo: any = {};
  tokenID: string = '';

  msgs: Message[] = [];
    
  items: MenuItem[];

  itemjitheader: any = [
    { label: 'Item Number', sort: 'item_number' },
    { label: 'Description', sort: 'description' },
    { label: 'Material', sort: 'material' },
    { label: 'Color', sort: 'color' },
    { label: 'Size', sort: 'size' },
    { label: 'Remark Design', sort: 'remark_design' },
    { label: 'Update', sort: 'update' },
  ];
  itemjitcolname: any = [
    'item_number',
    'description',
    'material',
    'color',
    'size',
    'remark_design',
    'update',
  ];
  itemjitcolhalign: any = [
    '',
    '',
    'p-text-center',
    '',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  // itemjitcolwidth: any = [
  //   '',
  //   '',
  //   { width: '110px' },
  //   { width: '170px' },
  //   { width: '110px' },
  //   { width: '170px' },
  // ];
  itemjitcolwidth: any = [
    '',
    '',
    '',
    '',
    { width: '170px' },
    '',
    '',
  ];
  // orgcollinghref:any = {'url':'#','label':'Application'}
  itemjitactionbtn: any = [1, 1, 1, 1, 1, 1];
  itemjitaddbtn = { route: 'detail', label: 'Create' };

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private specQouteService: SpecquoteService,
    private aclMenuService : AclmenucheckerService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.items = [
      {label: 'Select Columns', icon: 'pi pi-refresh', command: () => {
          this.update();
      }},
      {label: 'Filter', icon: 'pi pi-times', command: () => {
          this.delete();
      }},
      {label: 'Rows Per Page', icon: 'pi pi-times', command: () => {
          this.delete();
      }},
      {label: 'Format', icon: 'pi pi-times', command: () => {
          this.delete();
      }},
      {label: 'Flashback', icon: 'pi pi-times', command: () => {
          this.delete();
      }},
      {label: 'Save Report', icon: 'pi pi-times', command: () => {
          this.delete();
      }},
      {label: 'Reset', icon: 'pi pi-times', command: () => {
          this.delete();
      }},
      {label: 'Help', icon: 'pi pi-times', command: () => {
          this.delete();
      }},
      {label: 'Download', icon: 'pi pi-times', command: () => {
          this.delete();
      }},
     
      // {label: 'Setup', icon: 'pi pi-cog', routerLink: ['/setup']}
  ];

    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Spec & Quotation' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.itemJitInfo = value.data;
      this.tokenID = value.tokenId;
      this.aclMenuService.setAllMenus(this.itemJitInfo.sidemenus).then(data => {
        console.log("MENU ALL ACL Set");
        this.aclMenuService.getAclMenu(this.router.url).then(dataacl =>{
          if(JSON.stringify(dataacl.acl) === "{}"){
            console.log("No ACL Founded")
          } else {
            console.log("ACL Founded");
            console.log(dataacl.acl);
            this.itemjitactionbtn[0] = dataacl.acl.create;
              this.itemjitactionbtn[1] = dataacl.acl.read;
              this.itemjitactionbtn[2] = dataacl.acl.update;
              this.itemjitactionbtn[3] = dataacl.acl.delete;
              this.itemjitactionbtn[4] = dataacl.acl.view;
              this.itemjitactionbtn[5] = dataacl.acl.approval;
          }
        });
      });
      
    });
    this.refreshingUser();
  }
  refreshingUser() {
    this.isFetching = true;
    this.authservice.whoAmi().subscribe((data: BackendResponse) => {
      // console.log(">>>>>>> "+JSON.stringify(data));
      if ((data.status = 200)) {
        this.specQouteService.retrivespecquote().subscribe((orgall: BackendResponse) => {
          // console.log('>>>>>>> ' + JSON.stringify(orgall));
          this.itemjitlist = orgall.data;
          if (this.itemjitlist.length < 1) {
            let objtmp = {
              fullname: 'No records',
              userid: 'No records',
              active: 'No records',
              groupname: 'No records',
            };
            this.itemjitlist = [];
            this.itemjitlist.push(objtmp);
          }
          this.isFetching = false;
        });
      }
    });
  }

  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedItemJit = data;
  }

  viewData(data: any) {
    console.log(data);
    this.viewDisplay = true;
    this.selectedItemJit = data;
  }

  deleteUser() {
    console.log(this.selectedItemJit);
    let user = this.selectedItemJit;
    const payload = { user };
    this.specQouteService
      .deletespecquote(payload)
      .subscribe((resp: BackendResponse) => {
        console.log(resp);
        if (resp.status === 200) {
          this.showTopSuccess(resp.data);
        }
        this.display = false;
        this.refreshingUser();
      });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }


  save(severity: string) {
    this.messageService.add({severity:severity, summary:'Success', detail:'Data Saved'});
  }

  update() {
      this.messageService.add({severity:'success', summary:'Success', detail:'Data Updated'});
  }

  delete() {
      this.messageService.add({severity:'success', summary:'Success', detail:'Data Deleted'});
  }

}