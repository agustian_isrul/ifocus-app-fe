import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecquoteComponent } from './specquote.component';

describe('SpecquoteComponent', () => {
  let component: SpecquoteComponent;
  let fixture: ComponentFixture<SpecquoteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecquoteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecquoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
