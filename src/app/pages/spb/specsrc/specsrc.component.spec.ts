import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecsrcComponent } from './specsrc.component';

describe('SpecsrcComponent', () => {
  let component: SpecsrcComponent;
  let fixture: ComponentFixture<SpecsrcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecsrcComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecsrcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
