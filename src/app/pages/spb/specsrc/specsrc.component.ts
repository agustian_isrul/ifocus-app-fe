import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/services/spb/customer.service';
import { Capa } from 'src/app/services/spb/customer';
import { MenuItem, MessageService, SelectItem } from 'primeng/api';

@Component({
  selector: 'app-specsrc',
  templateUrl: './specsrc.component.html',
  styleUrls: ['./specsrc.component.scss']
})
export class SpecsrcComponent implements OnInit {
  capa: Capa[];
  capa1: Capa[];
  capa2: Capa[];
  clonedCapa: { [s: string]: Capa; } = {};

  first = 0;
  rows = 10;

  statuses: any[];
  loading: boolean = true;

  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching:boolean = false;

  statusess: SelectItem[];

  constructor(
    private customerService: CustomerService,
    public messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/mgm/home'};
    this.breadcrumbs = [
      { label: 'Spec SRC' }
    ];
    this.customerService.getCustomersSmall().then(capa => {
      this.capa = capa;
      this.loading = false;
    });
    this.statuses = [
      {label: 'Open', value: 'open'},
      {label: 'Confirm', value: 'confirm'},
      {label: 'Cancelled', value: 'cancel'},
  ]
  }
  onRowEditInit(capa: Capa) {
    this.capa[capa.id] = {...capa};
  }
  onRowEditSave(capa: Capa) {
    if (capa.price > 0) {
        delete this.capa[capa.id];
        this.messageService.add({severity:'success', summary: 'Success', detail:'Product is updated'});
    }  
    else {
        this.messageService.add({severity:'error', summary: 'Error', detail:'Invalid Price'});
    }
}
onRowEditCancel(capa: Capa, index: number) {
  this.capa2[index] = this.clonedCapa[capa.id];
  delete this.capa2[capa.id];
}
}
