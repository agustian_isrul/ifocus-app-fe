import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PointerfaceComponent } from './pointerface.component';

describe('PointerfaceComponent', () => {
  let component: PointerfaceComponent;
  let fixture: ComponentFixture<PointerfaceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PointerfaceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PointerfaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
