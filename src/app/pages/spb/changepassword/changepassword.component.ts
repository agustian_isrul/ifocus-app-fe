import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { PasswordModule } from 'primeng/password'

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.scss']
})
export class ChangepasswordComponent implements OnInit {

  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching:boolean = false;
  value: string;
  items: MenuItem[];

  constructor(
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/mgm/home'};
    this.breadcrumbs = [
      { label: 'Change Password' }
    ];
  }
  save(severity: string) {
    this.messageService.add({severity: severity, summary:'Success', detail:'Data Updated'});
}
  cancel() {
  this.messageService.add({severity:'cancel', summary:'Cancel', detail:'Cancel'});
}
}
