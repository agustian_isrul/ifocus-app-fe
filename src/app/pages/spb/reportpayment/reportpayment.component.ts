import { Component, OnInit } from '@angular/core';

interface City {
  name: string,
  code: string
}

@Component({
  selector: 'app-reportpayment',
  templateUrl: './reportpayment.component.html',
  styleUrls: ['./reportpayment.component.scss']
})
export class ReportpaymentComponent implements OnInit {
  loading = [false, false]
  company: City[];
  selectedCity1: City;
  fromDate: Date;
  toDate: Date;
  
  constructor() { 
    this.company = [
      // {name: 'New York', code: 'NY'},
      // {name: 'Rome', code: 'RM'},
      // {name: 'London', code: 'LDN'},
      // {name: 'Istanbul', code: 'IST'},
      // {name: 'Paris', code: 'PRS'}
  ];
  }

  ngOnInit(): void {
  }


  load(index) {
      this.loading[index] = true;
      setTimeout(() => this.loading[index] = false, 1000);
  }
 
  search(index) {
    this.loading[index] = true;
    setTimeout(() => this.loading[index] = false, 1000);
}
}
