import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForumcategoryComponent } from './forumcategory.component';

describe('ForumcategoryComponent', () => {
  let component: ForumcategoryComponent;
  let fixture: ComponentFixture<ForumcategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForumcategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumcategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
