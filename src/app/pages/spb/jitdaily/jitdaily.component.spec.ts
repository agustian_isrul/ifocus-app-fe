import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JitdailyComponent } from './jitdaily.component';

describe('JitdailyComponent', () => {
  let component: JitdailyComponent;
  let fixture: ComponentFixture<JitdailyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JitdailyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JitdailyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
