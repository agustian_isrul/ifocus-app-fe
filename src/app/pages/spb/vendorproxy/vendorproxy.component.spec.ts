import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorproxyComponent } from './vendorproxy.component';

describe('VendorproxyComponent', () => {
  let component: VendorproxyComponent;
  let fixture: ComponentFixture<VendorproxyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VendorproxyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorproxyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
