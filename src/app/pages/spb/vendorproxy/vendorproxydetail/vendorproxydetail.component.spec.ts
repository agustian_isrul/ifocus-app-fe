import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorproxydetailComponent } from './vendorproxydetail.component';

describe('VendorproxydetailComponent', () => {
  let component: VendorproxydetailComponent;
  let fixture: ComponentFixture<VendorproxydetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VendorproxydetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorproxydetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
