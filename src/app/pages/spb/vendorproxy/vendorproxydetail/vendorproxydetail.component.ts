import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { GroupServiceService } from 'src/app/services/root/group-service.service';
import { ItemjitService } from 'src/app/services/spb/itemjit.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-vendorproxydetail',
  templateUrl: './vendorproxydetail.component.html',
  styleUrls: ['./vendorproxydetail.component.scss']
})
export class VendorproxydetailComponent implements OnInit {
  extraInfo: any = {};
  isEdit: boolean = false;
  userId: any = null;
  stateOptions: any[] = [];
  stateOptionsEdit: any[] = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo: any = {};
  selectedApps: any = [];
  tokenID: string = '';
  groupForm!: FormGroup;
  submitted = false;
  orgsData: any[] = [];
  appInfoActive: any = {};
  orgSuggest: any = {};
  user: any = {};
  formatedOrg: any[] = [];
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private itemJitService: ItemjitService,
    private authservice: AuthService,
    private groupService: GroupServiceService,
    private messageService: MessageService,
    private location: Location,
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    let checkurl = this.extraInfo.indexOf('%23') !== -1 ? true : false;
    console.log('>>>>>>>>>>> ' + this.extraInfo);
    console.log(checkurl);
    if (checkurl) this.isEdit = true;
  }

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      {
        label: 'Vendor Proxy',
        command: (event) => {
          this.location.back();
        },
        url: '',
      },
      { label: this.isEdit ? 'Edit data' : 'Add data' },
    ];
    this.stateOptions = [
      { label: '- SELECT -', value: 1 },
      // { label: 'Activision link', value: 0 },
    ];
    this.stateOptionsEdit = [
      { label: '- SELECT -', value: 1 },
      // { label: 'Deactive', value: 0 },
    ];
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      this.groupForm = this.formBuilder.group({
        itemnumber: ['', Validators.required],
        description: ['', Validators.required],
        uom: ['', Validators.required],
        suppliername: ['', Validators.required],
        organization: [[], Validators.required],
        packsize: ['', Validators.required],
        category: ['', Validators.required],
        itemtype: ['', Validators.required],
        qcchecking: ['', Validators.required],
        ltrelease: ['', Validators.required],
        ltsend: ['', Validators.required],
        lttotal: ['', Validators.required],
        ropvendor: ['', Validators.required],
        ropmultipler: ['', Validators.required],
        leadtime: ['', Validators.required],
      });
      this.groupService.getAllGroupForData().subscribe((grpall: BackendResponse) => {
        this.orgsData = grpall.data.userGroup;
        // console.log(JSON.stringify(this.orgsData));
        this.formatOrgData(this.orgsData);
      });

      if (this.isEdit) {
        if (this.activatedRoute.snapshot.paramMap.get('id')) {
          this.userId = this.activatedRoute.snapshot.paramMap.get('id');
          this.itemJitService
            .retriveItemJitById(this.userId)
            .subscribe(async (result: BackendResponse) => {
              console.log('Data edit user ' + JSON.stringify(result.data));
              // if (this.isOrganization) {
              this.user.itemnumber = result.data.itemnumberid;
              this.user.suppliername = result.data.suppliername;
              this.user.uom = result.data.uom;
              this.user.description = result.data.description;
              this.user.operatingunit = result.data.operatingunit;
              this.user.organization = result.data.organization;
              this.user.packsize = result.data.packsize;
              this.user.category = result.data.category;
              this.user.itemtype = result.data.itemtype;
              this.user.qcchecking = result.data.qcchecking;
              this.user.ltrelease = result.data.ltrelease;
              this.user.ltsend = result.data.ltsend;
              this.user.lttotal = result.data.lttotal;
              this.user.ropvendor = result.data.ropvendor;
              this.user.ropmultiplier = result.data.ropmultiplier;
              this.user.leadtime = result.data.leadtime;
              this.groupForm.patchValue({
                itemnumber:  this.user.itemnumber,
                suppliername: this.user.suppliername,
                uom: this.user.uom,
                operatingunit: this.user.operatingunit,
                organization: this.user.organization,
                packsize: this.user.packsize,
                category: this.user.category,
                itemtype: this.user.itemtype,
                qcchecking: this.user.qcchecking,
                ltrelease: this.user.ltrelease,
                ltsend: this.user.ltsend,
                lttotal: this.user.lttotal,
                ropvendor: this.user.ropvendor,
                ropmultiplier: this.user.ropmultiplier,
                leadtime: this.user.leadtime,
                description: this.user.description,
              });
              this.groupForm.controls['itemnumber'].disable();
              console.log(this.user);
            });
        }
      }
    });
  }
  get f() {
    return this.groupForm.controls;
  }
  formatOrgData(data: any) {
    data.map((dt: any) => {
      let formated: any = {};
      formated.name = dt.groupname;
      formated.id = dt.id;
      this.formatedOrg.push(formated);
    });
  }

  filterOrg(event: any) {
    let filtered: any[] = [];
    let query = event.query;

    for (let i = 0; i < this.formatedOrg.length; i++) {
      let country = this.formatedOrg[i];
      if (country.name.toLowerCase().indexOf(query.toLowerCase()) > -1) {
        filtered.push(country);
      }
    }

    this.orgSuggest = filtered;
  }

  onSubmit() {
    this.submitted = true;
    console.log('>>>>>>>>IS NULL >>>> ' + this.leveltenant);
    // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
    if (this.groupForm.valid) {
      // event?.preventDefault;
      let payload = {};

      if (!this.isEdit) {
        payload = {
          itemnumber:  this.groupForm.get('itemnumber')?.value,
          suppliername: this.groupForm.get('suppliername')?.value,
          uom: this.groupForm.get('uom')?.value,
          operatingunit: this.groupForm.get('operatingunit')?.value,
          organization: this.groupForm.get('organization')?.value,
          packsize: this.groupForm.get('packsize')?.value,
          category: this.groupForm.get('category')?.value,
          itemtype: this.groupForm.get('itemtype')?.value,
          qcchecking: this.groupForm.get('qcchecking')?.value,
          ltrelease: this.groupForm.get('ltrelease')?.value,
          ltsend: this.groupForm.get('property')?.value,
          lttotal: this.groupForm.get('lttotal')?.value,
          ropvendor: this.groupForm.get('ropvendor')?.value,
          ropmultiplier: this.groupForm.get('ropmultiplier')?.value,
          leadtime: this.groupForm.get('leadtime')?.value,
          description: this.groupForm.get('description')?.value,
        };
        console.log('>>>>>>>> payload ' + JSON.stringify(payload));
        this.itemJitService.insertItemJit(payload).subscribe(
          (result: BackendResponse) => {
            if (result.status === 200) {
              this.location.back();
            } (err) => {
              console.log(err);
              this.showTopCenterErr(err.error.data);
            }
          },
          (err) => {
            console.log(err);
            this.showTopCenterErr(err.error.data);
          }
        );
      } else {
        payload = {
          itemnumber:  this.userId,
          suppliername: this.groupForm.get('suppliername')?.value,
          uom: this.groupForm.get('uom')?.value,
          operatingunit: this.groupForm.get('operatingunit')?.value,
          organization: this.groupForm.get('organization')?.value,
          packsize: this.groupForm.get('packsize')?.value,
          category: this.groupForm.get('category')?.value,
          itemtype: this.groupForm.get('itemtype')?.value,
          qcchecking: this.groupForm.get('qcchecking')?.value,
          ltrelease: this.groupForm.get('ltrelease')?.value,
          ltsend: this.groupForm.get('property')?.value,
          lttotal: this.groupForm.get('lttotal')?.value,
          ropvendor: this.groupForm.get('ropvendor')?.value,
          ropmultiplier: this.groupForm.get('ropmultiplier')?.value,
          leadtime: this.groupForm.get('leadtime')?.value,
          description: this.groupForm.get('description')?.value,
        };
        console.log('>>>>>>>> payload ' + JSON.stringify(payload));
        this.itemJitService
          .updateItemJit(payload)
          .subscribe((result: BackendResponse) => {
            // console.log(">>>>>>>> return "+JSON.stringify(result));
            if (result.status === 200) {
              this.location.back();
            }
          });
      }
    }

    console.log(this.groupForm.valid);
  }
  showTopCenterErr(message: string) {
    this.messageService.add({
      severity: 'error',
      summary: 'Error',
      detail: message,
    });
  }
}

