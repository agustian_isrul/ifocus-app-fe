import { Component, OnInit } from '@angular/core';
import { Breadcrumb } from 'primeng/breadcrumb';
import { MenuItem } from 'primeng/api';
import { CustomerService } from 'src/app/services/spb/customer.service';
import { Capa } from 'src/app/services/spb/customer';

interface Company {
  name: string,
  value: string
}

interface Bank {
  name: string
}

@Component({
  selector: 'app-invoicepo',
  templateUrl: './invoicepo.component.html',
  styleUrls: ['./invoicepo.component.scss']
})
export class InvoicepoComponent implements OnInit {
  capa: Capa[];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  first = 0;
  rows = 10;

  company: Company[];
  selectedCompany: Company;

  bank: Bank[];
  selectedBank: Bank;

  checkboxStatus: any;

  pencet:boolean=false;
  loading: boolean = true;
  dialog: boolean;

  constructor(
    private customerService : CustomerService
  ) { }

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home'};
    this.breadcrumbs = [
      { label: 'Invoice PO'}
    ]

    this.company = [
      {name: 'Dexa Medica, PT', value: 'dexa'},
      {name: 'Ferron Par Pharmaceuticals, PT', value: 'ferron'},
      {name: 'Anugrah Argon Medica, PT', value: 'anugrah'},
      {name: 'Argon Group, PT', value: 'argon'}
    ];
    this.bank = [
      {name: 'BUDHI no. 2193719823 Bank Central Asia'},
      {name: 'BUDHI no. 2193721385 Bank Central Asia'},
      {name: 'BUDHI no. 2121831341 Bank Central Asia'},
      {name: 'BUDHI no. 2318471203 Bank Central Asia'}
    ];
    this.customerService.getCustomersSmall().then(capa => this.capa = capa);
    this.loading = false;
    
    this.capa.forEach(
      capa => (capa.date = new Date(capa.date))
    );
  }
  handleChange(event: any): void {
    console.log('Info : Come From handlechange');
    console.log(event.target.files);
  }
  tooglePencet(){
    this.pencet=!this.pencet
  }
  openNew(){
    this.dialog = true;
  }
  editDialog(){
    this.dialog = true;
  }
  hideDialog(){
    this.dialog = false;
  }
  saveDialog(){
    this.dialog = false;
  }

}
