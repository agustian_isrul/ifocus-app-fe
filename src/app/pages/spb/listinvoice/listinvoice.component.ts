import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/services/spb/customer.service';
import { Capa } from 'src/app/services/spb/customer';
import { MenuItem, MessageService, SelectItem } from 'primeng/api';

@Component({
  selector: 'app-listinvoice',
  templateUrl: './listinvoice.component.html',
  styleUrls: ['./listinvoice.component.scss']
})
export class ListinvoiceComponent implements OnInit {
  capa: Capa[];
  first = 0;
  rows = 10;

  loading: boolean = true;

  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching:boolean = false;

  statuses: any[];

  constructor(
    private customerService: CustomerService
  ) { }

  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/mgm/home'};
    this.breadcrumbs = [
      { label: 'List Invoice' }
    ];
    
    this.customerService.getCustomersSmall().then(capa => this.capa = capa);
    this.loading = false;

    this.capa.forEach(
      capa => (capa.date = new Date(capa.date))
    );
    this.statuses = [
      {label: 'New', value: 'new'},
      {label: 'Cancelled', value: 'cancelled'},
      {label: 'Completed', value: 'completed'},
      {label: 'Not Accept', value: 'notaccept'},
      {label: 'To Be Processed', value: 'tbprocessed'},
      {label: 'Processed', value: 'processed'}
  ]
    }
    next() {
      this.first = this.first + this.rows;
  }

  prev() {
      this.first = this.first - this.rows;
  }

  reset() {
      this.first = 0;
  }

  isLastPage(): boolean {
      return this.capa ? this.first === (this.capa.length - this.rows): true;
  }

  isFirstPage(): boolean {
      return this.capa ? this.first === 0 : true;
  }
  }

