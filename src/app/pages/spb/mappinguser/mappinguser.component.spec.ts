import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MappinguserComponent } from './mappinguser.component';

describe('MappinguserComponent', () => {
  let component: MappinguserComponent;
  let fixture: ComponentFixture<MappinguserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MappinguserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MappinguserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
