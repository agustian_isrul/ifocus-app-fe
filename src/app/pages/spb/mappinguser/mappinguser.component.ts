import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/services/spb/customer.service';
import { Capa } from 'src/app/services/spb/customer';
import { MenuItem, MessageService, SelectItem } from 'primeng/api';


@Component({
  selector: 'app-mappinguser',
  templateUrl: './mappinguser.component.html',
  styleUrls: ['./mappinguser.component.scss']
})
export class MappinguserComponent implements OnInit {
  capa: Capa[];
  capa1: Capa[];
  capa2: Capa[];
  clonedCapa: { [s: string]: Capa; } = {};

  first = 0;
  rows = 10;

  statuses: any[];
  loading: boolean = true;

  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching:boolean = false;

  statusess: SelectItem[];

  constructor(
    private customerService: CustomerService,
    public messageService: MessageService
  ) { }

  ngOnInit(): void {

    this.statusess = [{label: 'Open', value: 'OPEN'},{label: 'Close', value: 'CLOSE'}]
    

    this.home = {icon: 'pi pi-home', routerLink: '/mgm/home'};
    this.breadcrumbs = [
      { label: 'Mapping User List' }
    ];
    
    this.customerService.getCustomersSmall().then(capa => {
      this.capa = capa;
      this.loading = false;
    });
    this.statuses = [
      {label: 'Open', value: 'open'},
      {label: 'Close', value: 'close'}
    ]

    }

    onRowEditInit(capa: Capa) {
      this.capa[capa.id] = {...capa};
  }

  onRowEditSave(capa: Capa) {
    if (capa.price > 0) {
        delete this.capa[capa.id];
        this.messageService.add({severity:'success', summary: 'Success', detail:'Product is updated'});
    }  
    else {
        this.messageService.add({severity:'error', summary: 'Error', detail:'Invalid Price'});
    }
}

onRowEditCancel(capa: Capa, index: number) {
  this.capa2[index] = this.clonedCapa[capa.id];
  delete this.capa2[capa.id];
}

    next() {
        this.first = this.first + this.rows;
    }

    prev() {
        this.first = this.first - this.rows;
    }

    reset() {
        this.first = 0;
    }

    isLastPage(): boolean {
        return this.capa ? this.first === (this.capa.length - this.rows): true;
    }

    isFirstPage(): boolean {
        return this.capa ? this.first === 0 : true;
    }
  }
