import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateplanningorderComponent } from './createplanningorder.component';

describe('CreateplanningorderComponent', () => {
  let component: CreateplanningorderComponent;
  let fixture: ComponentFixture<CreateplanningorderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateplanningorderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateplanningorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
