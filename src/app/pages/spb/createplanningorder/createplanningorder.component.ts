import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/services/spb/customer.service';
import { Customer, Representative, IDepartment, TableUtil } from 'src/app/services/spb/customer';
import { MessageService, SelectItem, SelectItemGroup } from 'primeng/api';
import { Capa } from 'src/app/services/spb/customer';
import { PrimeNGConfig } from 'primeng/api';
import { MenuItem } from 'primeng/api'

interface Report {
  namee: string,
}

interface Supplier {
  name : string,
  value : string
}

interface Jit {
  name : string,
  value : string
}

@Component({
  selector: 'app-createplanningorder',
  templateUrl: './createplanningorder.component.html',
  styleUrls: ['./createplanningorder.component.scss']
})
export class CreateplanningorderComponent implements OnInit {
  capa: Capa[];
  first = 0;
  rows = 10;

  statuses: any[];
  loading: boolean = true;

  items: SelectItem[];
  item: string;

  report: Report[];
  selectedReport: Report;

  groupedReports: SelectItemGroup[];
  selectedReports: string;

  groupedActions: SelectItemGroup[];
  selectedActions: string;

  groupedCities: SelectItemGroup[];
  selectedCity3: string;

  itemss: MenuItem[];
  itemsa: MenuItem[];

  customers: Customer[];

  representative: Representative[];

  exportColumns: any[];

  supplier: Supplier[];
  selectedSupplier: Supplier;

  jit: Jit[];
  selectedJit: Jit;

  breadcrumbs!: MenuItem[];
  home!: MenuItem;

  public departments:Array<IDepartment> = [
    {Id:1, Name:'Control Break'},
    {Id:2, Name:'Highlight'},
    {Id:3, Name:'Group By'}
  ]
 
   public deptId:number;
 
   countries: any[];
   selectedAction: any;
   selectedCity1: any;

  constructor(
    private customerService: CustomerService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home'};
    this.breadcrumbs = [
      { label: 'Create Planning Order'}
    ]

    this.customerService.getCustomersMedium().then(customers => {
    this.customers = customers;
    this.loading = false;

    // this.customers.forEach(
    //   customer => (customer.date = new Date(customer.date))
    // );
  });

  this.supplier = [
    {name: 'ALLFORM INDONESIA, PT', value: 'ai'},
    {name: 'ALTINEX, PT', value: 'al'},
    {name: 'ANCOL TERANG METAL PRINTING INDUSTRI, PT', value:'atmpi'},
    {name: 'ANINDOJAYA SWAKARYA, PT', value: 'as'},
    {name: 'ARISTROCAT CIPTA MANDIRI, PT', value: 'acm'}
];

this.jit = [
  {name: 'Non JIT', value: 'non'},
  {name: 'JIT', value: 'al'},
];

  this.capa.forEach(
    capa => (capa.date = new Date(capa.date))
  );
  this.statuses = [
    {label: 'Open', value: 'open'},
    {label: 'Close', value: 'close'}
  ];

  }
  
  }
  


