import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpbhomeComponent } from './spbhome.component';

describe('SpbhomeComponent', () => {
  let component: SpbhomeComponent;
  let fixture: ComponentFixture<SpbhomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpbhomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpbhomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
