export interface Customer {
    id?: number;
    line?: string;
    item_number?: number;
    item_description?: string;
    uom?: string;
    currency?: string;
    unit_price?: string;
    quantity?: string;
    amount?: string;
    ship_to?: string;
    arrival_at_wh?: string;
    arrival_at_site?: string;
    est_arrival_at_site?: string;
    po_type?: string;
    note_to_supplier?: string;
    attachment?: string;
    confirm_date?: string;
    loe?: string;
    status?: string;
}
export interface Openpodata {
    id?: number;
    line?: string;
    item_number?: number;
    item_description?: string;
    uom?: string;
    currency?: string;
    unit_price?: string;
    quantity?: string;
    amount?: string;
    ship_to?: string;
    arrival_at_wh?: string;
    arrival_at_site?: string;
    est_arrival_at_site?: string;
    po_type?: string;
    note_to_supplier?: string;
    attachment?: string;
    confirm_date?: string;
    loe?: string;
    status?: string;
}