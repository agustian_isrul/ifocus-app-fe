import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Customer } from './customer';

@Injectable()
export class CustomerService {
    constructor(private http: HttpClient) { }

    getCustomersLarge() {
        return this.http.get<any>('assets/openpo-dummy.json')
            .toPromise()
            .then(res => <Customer[]>res.openpo1)
            .then(openpo1 => { return openpo1; });
    }
    getOpenpoSmall() {
        return this.http.get<any>('assets/openpo-dummy.json')
            .toPromise()
            .then(res => <Customer[]>res.openpo2)
            .then(openpo2 => { return openpo2; });
    }
}