import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopenpoComponent } from './topenpo.component';

describe('TopenpoComponent', () => {
  let component: TopenpoComponent;
  let fixture: ComponentFixture<TopenpoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopenpoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopenpoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
