import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TcapaComponent } from './tcapa.component';

describe('TcapaComponent', () => {
  let component: TcapaComponent;
  let fixture: ComponentFixture<TcapaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TcapaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TcapaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
