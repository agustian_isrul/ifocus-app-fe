import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/services/spb/customer.service';
import { Capa } from 'src/app/services/spb/customer';

@Component({
  selector: 'app-tcapa',
  templateUrl: './tcapa.component.html',
  styleUrls: ['./tcapa.component.scss']
})
export class TcapaComponent implements OnInit {

  capa: Capa[];
  first = 0;
  rows = 10;

  statuses: any[];
  loading: boolean = true;

  constructor(private customerService: CustomerService) { }

  ngOnInit(): void {
    this.customerService.getCustomersSmall().then(capa => this.capa = capa);
    this.loading = false;

    this.capa.forEach(
      capa => (capa.date = new Date(capa.date))
    );
    this.statuses = [
      {label: 'Open', value: 'open'},
      {label: 'Close', value: 'close'}
    ]
    }

    next() {
        this.first = this.first + this.rows;
    }

    prev() {
        this.first = this.first - this.rows;
    }

    reset() {
        this.first = 0;
    }

    isLastPage(): boolean {
        return this.capa ? this.first === (this.capa.length - this.rows): true;
    }

    isFirstPage(): boolean {
        return this.capa ? this.first === 0 : true;
    }
  
}
