import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TplanningorderComponent } from './tplanningorder.component';

describe('TplanningorderComponent', () => {
  let component: TplanningorderComponent;
  let fixture: ComponentFixture<TplanningorderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TplanningorderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TplanningorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
