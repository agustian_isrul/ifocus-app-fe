import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/services/spb/customer.service';
import { Customer, Representative, IDepartment, TableUtil } from 'src/app/services/spb/customer';
import { MessageService, SelectItem, SelectItemGroup } from 'primeng/api';
import { Capa } from 'src/app/services/spb/customer';
import { PrimeNGConfig } from 'primeng/api';
import { MenuItem } from 'primeng/api'

interface Report {
  namee: string,
}

@Component({
  selector: 'app-tplanningorder',
  templateUrl: './tplanningorder.component.html',
  styleUrls: ['./tplanningorder.component.scss']
})
export class TplanningorderComponent implements OnInit {

  capa: Capa[];
  first = 0;
  rows = 10;

  statuses: any[];
  loading: boolean = true;

  items: SelectItem[];
  item: string;

  report: Report[];
  selectedReport: Report;

  groupedReports: SelectItemGroup[];
  selectedReports: string;

  groupedActions: SelectItemGroup[];
  selectedActions: string;

  groupedCities: SelectItemGroup[];
  selectedCity3: string;

  itemss: MenuItem[];
  itemsa: MenuItem[];

  customers: Customer[];

  representative: Representative[];

  exportColumns: any[];

 public departments:Array<IDepartment> = [
   {Id:1, Name:'Control Break'},
   {Id:2, Name:'Highlight'},
   {Id:3, Name:'Group By'}
 ]

  public deptId:number;

  countries: any[];
  selectedAction: any;
  selectedCity1: any;

  constructor(
    private customerService: CustomerService,
    private messageService: MessageService
     ) { }

  ngOnInit(): void {
    
    this.itemss = [
      {label: 'Primary Report', icon: 'pi pi-check', command: () => {
          this.update();
      }}
  ];

  this.itemsa = [
    {label: 'Outstanding PR', icon: 'pi pi-check', command: () => {
      this.update();
  }},
  {
    label: 'Outstanding PR by Item Category', icon: 'pi pi-check', command: () => {
      this.update();
  }},
  {
    label: 'Planning Order', icon: 'pi pi-check', command: () => {
      this.update();
  }}
  ];

    this.customerService.getCustomersMedium().then(customers => {
    this.customers = customers;
    this.loading = false;

    // this.customers.forEach(
    //   customer => (customer.date = new Date(customer.date))
    // );
  });

    this.groupedReports = [
      {
        label: 'Default', value: 'de',
        items: [
          {label: '1. Primary Report', value: 'pr'},
        ]
      },
      {
        label: 'Public', value: 'pu',
        items: [
          {label: "1. Outstanding PR", value: 'opr'},
          {label: "2. Outstanding PR by Item Category", value: 'opri'},
          {label: "3. Planning Order", value: 'po'}
        ]
      }
    ]

    this.capa.forEach(
      capa => (capa.date = new Date(capa.date))
    );
    this.statuses = [
      {label: 'Open', value: 'open'},
      {label: 'Close', value: 'close'}
    ]

    }

  save(severity: string) {
      this.messageService.add({severity: severity, summary:'Success', detail:'Data Saved'});
  }
  
  update() {
      this.messageService.add({severity:'success', summary:'Success', detail:'Data Updated'});
  }
  
  delete() {
      this.messageService.add({severity:'success', summary:'Success', detail:'Data Deleted'});
  }
  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(this.customers);
      const workbook = { Sheets: { data: worksheet }, SheetNames: ["data"] };
      const excelBuffer: any = xlsx.write(workbook, {
        bookType: "xlsx",
        type: "array"
      });
      this.saveAsExcelFile(excelBuffer, "customers");
    });
  }
  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE =
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
      let EXCEL_EXTENSION = ".xlsx";
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(
        data,
        fileName + "_export_" + new Date().getTime() + EXCEL_EXTENSION
      );
    });
  }
  exportTable() {
    TableUtil.exportTableToExcel("ExampleMaterialTable");
  }

  exportNormalTable() {
    TableUtil.exportTableToExcel("ExampleNormalTable");
  }
}
