import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TnotifikasiComponent } from './tnotifikasi.component';

describe('TnotifikasiComponent', () => {
  let component: TnotifikasiComponent;
  let fixture: ComponentFixture<TnotifikasiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TnotifikasiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TnotifikasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
