import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { NotificationlistService } from 'src/app/services/spb/notificationlist.service';

import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';
// import { NotificationlistService } from 'src/app/services/spb/notificationlist.service';

@Component({
  selector: 'app-tnotifikasi',
  templateUrl: './tnotifikasi.component.html',
  styleUrls: ['./tnotifikasi.component.scss']
})
export class TnotifikasiComponent implements OnInit {

  // "id",
  // "idmodule",
  // "sbjmessage",
  // "receiveddate",
  // "usersenderid",
  // "userreceiveid",
  // "tenantsenderid",
  // "tenantreceiveid",
  // "status",
  // "created_by",
  // "created_at",
  // "updated_at",
  display = false;
  scrollheight:any ="400px"
  viewDisplay = false;
  viewApprove = false;
  selectedUser: any = {};
  isGroup = false;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  notiflist: any[] = [];
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = '';
  modultype: any = [];
  statustype: any = [];
  modultypeselected: any = [];
  statustypeselected: any = [];
  notifheader: any = [
    { label: 'Module', sort: 'modulename' },
    { label: 'Subject', sort: 'sbjmessage' },
    { label: 'Receive Date', sort: 'receiveddate' },
    { label: 'Status', sort: 'status' },
  ];
  notifcolname: any = [
    'modulename',
    'sbjmessage',
    'receiveddate',
    'status',
  ];
  notifcolhalign: any = [
    '',
    '',
    'p-text-center',
    'p-text-center',
  ];
  notifaddbtn = { route: 'detail', label: 'Add Data' };
  notifcolwidth: any = [
    { width: '220px' },
    { width: '600px' },
    { width: '170px' },
    { width: '100px' },
  ];
  // orgcollinghref:any = {'url':'#','label':'Application'}
  listactionbtn: any = [0, 0, 0, 0, 0, 0];
  // notifaddbtn = { route: 'detail', label: 'Add Data' };
  notffieldformat: any =['','',{code:'D', format:'dd-MMM-yy, hh:mm:ss'},''];
  constructor(
    public dialogService: DialogService,
    public messageService: MessageService,
    private authservice: AuthService,
    private aclMenuService : AclmenucheckerService,
    private notifikasiListService : NotificationlistService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Notification' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.aclMenuService.setAllMenus(this.userInfo.sidemenus).then(data => {
        console.log("MENU ALL ACL Set");
        this.aclMenuService.getAclMenu(this.router.url).then(dataacl =>{
          if(JSON.stringify(dataacl.acl) === "{}"){
            console.log("No ACL Founded")
          } else {
            console.log("ACL Founded");
            console.log(dataacl.acl);

            this.notifikasiListService
            .getModulType()
            .subscribe((response: BackendResponse) => {
              console.log(JSON.stringify(response));
              if (response.status == 200) {
                console.log(response.data);
                this.modultype = response.data;
              } else {
                this.modultype = [];
              }
            });
          }
        });
      });
      
      this.notifikasiListService.getAllModulNotification().subscribe((modulResults:BackendResponse)=>{
        console.log("Result from module service ", modulResults);
        if(modulResults.status == 200 ){
          this.modultype = modulResults.data
        } else {
          this.modultype=[];
        }
      });
      this.statustype = [{code:0, statusname:"Open"}, {code:1, statusname:"Done"}, {code:2, statusname:"Readed"}]
      this.refreshinNotify();
    });
   
  }

  refreshinNotify() {
    this.isFetching = true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
      // console.log(">>>>>>> "+JSON.stringify(data));
      // let objtmp = {
      //   idmodule: 'No records',
      //   sbjmessage: 'No records',
      //   receiveddate: 'No records',
      //   status: 'No records',
      // };
      // this.notiflist = [];
      // this.notiflist.push(objtmp);
      console.log(">>> Notify Info : "+JSON.stringify(this.notiflist));
      // if ((data.status = 200)) {
        this.notifikasiListService.getAll().subscribe((orgall: BackendResponse) => {
          // console.log('>>>>>>> ' + JSON.stringify(orgall));
          this.notiflist = orgall.data;
          if (this.notiflist.length < 1) {
            let objtmp = {
              modulename: 'No records',
              sbjmessage: 'No records',
              receiveddate: 'No records',
              status: 'No records',
            };
            this.notiflist = [];
            this.notiflist.push(objtmp);
            console.log(">>> Notify Info : "+JSON.stringify(this.notiflist));

          }
          this.isFetching = false;
        });
      // }
    // });
  }

  previewFilter(){
    let payload = {
      module: this.modultypeselected,
      status: this.statustypeselected
    };
    // this.notifikasiListService.getByModulStatus(payload).subscribe((orgall: BackendResponse) => {
    //   // console.log('>>>>>>> ' + JSON.stringify(orgall));
    //   this.notiflist = orgall.data;
    //   if (this.notiflist.length < 1) {
    //     let objtmp = {
    //       module: 'No records',
    //       subject: 'No records',
    //       receive_date: 'No records',
    //       status: 'No records',
    //     };
    //     this.notiflist = [];
    //     this.notiflist.push(objtmp);
    //   }
    //   this.isFetching = false;
    // });
    
  }
}