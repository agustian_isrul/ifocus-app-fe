import { Component, OnInit } from '@angular/core';

interface Supplier {
  name: string
}
interface Status {
  name: string
}

@Component({
  selector: 'app-tshipmentnotice',
  templateUrl: './tshipmentnotice.component.html',
  styleUrls: ['./tshipmentnotice.component.scss']
})
export class TshipmentnoticeComponent implements OnInit {

  suppliers: Supplier[];
  statuses: Status[];

  selectedSupplier: Supplier;
  selectedStatus: Status;

  constructor() {
    this.suppliers = [
      {name: 'Altinex'},
      {name: 'Dexa Medica'},
      {name: 'Feron',},
  ];
    this.statuses = [
      {name: 'All'},
      {name: 'Submitted'},
      {name: 'Not Submitted',},
  ];
   }

  ngOnInit(): void {
    //nothing
  }

}
