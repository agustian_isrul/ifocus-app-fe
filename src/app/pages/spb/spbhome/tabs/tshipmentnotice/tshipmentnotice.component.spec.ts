import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TshipmentnoticeComponent } from './tshipmentnotice.component';

describe('TshipmentnoticeComponent', () => {
  let component: TshipmentnoticeComponent;
  let fixture: ComponentFixture<TshipmentnoticeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TshipmentnoticeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TshipmentnoticeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
