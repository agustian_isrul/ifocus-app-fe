import { Component, OnInit, ViewChild } from '@angular/core';
import { OpenpoService } from 'src/app/services/spb/openpo.service';
import { Router } from '@angular/router';
import { MenuItem, MessageService, PrimeNGConfig } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';
import { Customer, Receipt } from './customer';
import { InvoicedummyService } from './customerservice';
import { Table } from 'primeng/table';

interface Supplier {
  name: string
}
interface Status {
  name: string
}

@Component({
  selector: 'app-tinvoice',
  templateUrl: './tinvoice.component.html',
  styleUrls: ['./tinvoice.component.scss']
})
export class TinvoiceComponent implements OnInit {

  customers: Customer[];
  receipts: Receipt[];

  selectedCustomers: Customer[];
  selectedReceipts: Receipt[];


  invStatuses: any[];

  loading: boolean = true;

  @ViewChild('dt') table: Table;

  suppliers: Supplier[];
  statuses: Status[];

  selectedSupplier: Supplier;
  selectedStatus: Status;

  display = false;
  scrollheight: any = "400px"
  viewDisplay = false;
  viewApprove = false;
  selectedUser: any = {};
  isGroup = false;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  openpolist: any[] = [];
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = '';
  modultype: any = [];
  statustype: any = [];
  modultypeselected: any = [];
  statustypeselected: any = [];
  openpoheader: any = [{
      label: 'Supplier Name',
      sort: 'module'
    },
    {
      label: 'PO Number',
      sort: 'subject'
    },
    {
      label: 'Receive Date',
      sort: 'receive_date'
    },
    {
      label: 'Status',
      sort: 'status'
    },
  ];
  openpocolname: any = [
    'module',
    'subject',
    'receive_date',
    'status',
  ];
  openpocolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  // notifcolhalign: any = [
  //   '',
  //   '',
  //   { width: '110px' },
  //   { width: '170px' },
  //   { width: '110px' },
  //   { width: '170px' },
  // ];
  openpocolwidth: any = [
    '',
    '',
    {
      width: '170px'
    },
    '',
  ];
  // orgcollinghref:any = {'url':'#','label':'Application'}
  listactionbtn: any = [1, 1, 1, 1];
  // notifaddbtn = { route: 'detail', label: 'Add Data' };

  constructor(
    private customerService: InvoicedummyService,
    private primengConfig: PrimeNGConfig,
    public dialogService: DialogService,
    public messageService: MessageService,
    private authservice: AuthService,
    private aclMenuService: AclmenucheckerService,
    private openPOService: OpenpoService,
    private router: Router
  ) {
    this.suppliers = [{
        name: 'Altinex'
      },
      {
        name: 'Dexa Medica'
      },
      {
        name: 'Feron',
      },
    ];
    this.statuses = [{
        name: 'All'
      },
      {
        name: 'Open'
      },
      {
        name: 'Close',
      },
    ];
  }

  ngOnInit(): void {
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.aclMenuService.setAllMenus(this.userInfo.sidemenus).then(data => {
        console.log("MENU ALL ACL Set");
        this.aclMenuService.getAclMenu(this.router.url).then(dataacl => {
          if (JSON.stringify(dataacl.acl) === "{}") {
            console.log("No ACL Founded")
          } else {
            console.log("ACL Founded");
            console.log(dataacl.acl);

            this.openPOService
              .getModulType()
              .subscribe((response: BackendResponse) => {
                console.log(JSON.stringify(response));
                if (response.status == 200) {
                  console.log(response.data);
                  this.modultype = response.data;
                } else {
                  this.modultype = [];
                }
              });
          }
        });
      });

    });

    this.customerService.getCustomersLarge().then(customers => {
      this.customers = customers;
      this.loading = false;
    });

    this.invStatuses = [{
        label: 'Draft',
        value: 'draft'
      },
      {
        label: 'Submitted',
        value: 'submitted'
      }
    ]

    this.customerService.getReceiptsSmall().then(receipts => {
      this.receipts = receipts;
      this.loading = false;
    });

    this.primengConfig.ripple = true;
    this.refreshingUser();
  }

  refreshingUser() {
    this.isFetching = true;
    this.authservice.whoAmi().subscribe((data: BackendResponse) => {
      // console.log(">>>>>>> "+JSON.stringify(data));
      let objtmp = {
        module: 'No records',
        subject: 'No records',
        receive_date: 'No records',
        status: 'No records',
      };
      this.openpolist = [];
      this.openpolist.push(objtmp);
      console.log(">>> User Info : " + JSON.stringify(this.openpolist));
      if ((data.status = 200)) {
        this.openPOService.getAll().subscribe((orgall: BackendResponse) => {
          // console.log('>>>>>>> ' + JSON.stringify(orgall));
          this.openpolist = orgall.data;
          if (this.openpolist.length < 1) {
            let objtmp = {
              module: 'No records',
              subject: 'No records',
              receive_date: 'No records',
              status: 'No records',
            };
            this.openpolist = [];
            this.openpolist.push(objtmp);
            console.log(">>> User Info : " + JSON.stringify(this.openpolist));

          }
          this.isFetching = false;
        });
      }
    });
  }

  previewFilter() {
    let payload = {
      module: this.modultypeselected,
      status: this.statustypeselected
    };
    this.openPOService.getByModulStatus(payload).subscribe((orgall: BackendResponse) => {
      // console.log('>>>>>>> ' + JSON.stringify(orgall));
      this.openpolist = orgall.data;
      if (this.openpolist.length < 1) {
        let objtmp = {
          module: 'No records',
          subject: 'No records',
          receive_date: 'No records',
          status: 'No records',
        };
        this.openpolist = [];
        this.openpolist.push(objtmp);
      }
      this.isFetching = false;
    });

  }
  onActivityChange(event) {
    const value = event.target.value;
    if (value && value.trim().length) {
      const activity = parseInt(value);

      if (!isNaN(activity)) {
        this.table.filter(activity, 'activity', 'gte');
      }
    }
  }

  onDateSelect(value) {
    this.table.filter(this.formatDate(value), 'date', 'equals')
  }

  formatDate(date) {
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (month < 10) {
      month = '0' + month;
    }

    if (day < 10) {
      day = '0' + day;
    }

    return date.getFullYear() + '-' + month + '-' + day;
  }
}