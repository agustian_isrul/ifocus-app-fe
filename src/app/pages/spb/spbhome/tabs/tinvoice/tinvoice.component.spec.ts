import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TinvoiceComponent } from './tinvoice.component';

describe('TinvoiceComponent', () => {
  let component: TinvoiceComponent;
  let fixture: ComponentFixture<TinvoiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TinvoiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TinvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
