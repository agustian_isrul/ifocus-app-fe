export interface Customer {
    id?: number;
    inv_number?: number;
    inv_amount?: string;
    inv_date?: string;
    inv_status?: string;
}

export interface Receipt {
    id?: number;
    oprec_number?: number;
    oprec_desc?: string;
    oprec_date?: string;
    oprec_total?: number;
}