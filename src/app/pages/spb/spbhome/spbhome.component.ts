import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-spbhome',
  templateUrl: './spbhome.component.html',
  styleUrls: ['./spbhome.component.scss']
})
export class SpbhomeComponent implements OnInit {
  items: MenuItem[];

    activeItem: MenuItem;
  constructor() { }

  ngOnInit(): void {
    this.items = [
      {label: 'NOTIFICATION', icon: 'pi pi-fw pi-bell', routerLink:'notification'},
      {label: 'PLANNING ORDER', icon: 'pi pi-fw pi-calendar', routerLink:'planningorder'},
      {label: 'OPEN PO', icon: 'pi pi-fw pi-pencil', routerLink:'openpo'},
      {label: 'SHIPMENT NOTICE', icon: 'pi pi-fw pi-send', routerLink:'shipmentnotice'},
      {label: 'CAPA', icon: 'pi pi-fw pi-cog', routerLink:'capa'},
      {label: 'INVOICE', icon: 'pi pi-fw pi-shopping-cart', routerLink:'invoice'}
  ];

    this.activeItem = this.items[0];
  }

  onChange(event) {
    this.activeItem = this.items[0];
  }

}
