import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, Message, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { UsermanagerService } from 'src/app/services/root/usermanager.service';
import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {
  display = false;
  scrollheight:any ="400px"
  viewDisplay = false;
  viewApprove = false;
  selectedUser: any = {};
  isGroup = false;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  userlist: any[] = [];
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = '';
  usrheader: any = [
    { label: 'Supplier', sort: 'supplier' },
    { label: 'PO No', sort: 'po_no' },
    { label: 'PO Line', sort: 'po_line' },
    { label: 'Invoice No', sort: 'invoice_no' },
    { label: 'Currency', sort: 'currency' },
    { label: 'Inv Date', sort: 'inv_date' },
    { label: 'Item Description', sort: 'item_desc' },
    { label: 'UOM', sort: 'uom' },
    { label: 'Price', sort: 'price' },
  ];
  usrcolname: any = [
    'supplier',
    'po_no',
    'po_line',
    'invoice_no',
    'currency',
    'inv_date',
    'item_desc',
    'uom',
    'price',
  ];
  usrcolhalign: any = [
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
  ];
  // usrcolwidth: any = [
  //   '',
  //   '',
  //   { width: '110px' },
  //   { width: '170px' },
  //   { width: '110px' },
  //   { width: '170px' },
  // ];
  usrcolwidth: any = [
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
  ];
  // orgcollinghref:any = {'url':'#','label':'Application'}
  usractionbtn: any = [0, 0, 0, 0, 1, 1, 1, 1];
  usraddbtn = { route: 'detail', label: 'Add Data' };

  msgs: Message[] = [];
    
  items: MenuItem[];
  
  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private userService: UsermanagerService,
    private aclMenuService : AclmenucheckerService,
    private router: Router
  ) {
    this.items = [
      {label: 'Select Columns', icon: 'pi pi-refresh', command: () => {
          this.update();
      }},
      {label: 'Filter', icon: 'pi pi-times', command: () => {
          this.delete();
      }},
      {label: 'Rows Per Page', icon: 'pi pi-times', command: () => {
          this.delete();
      }},
      {label: 'Format', icon: 'pi pi-times', command: () => {
          this.delete();
      }},
      {label: 'Flashback', icon: 'pi pi-times', command: () => {
          this.delete();
      }},
      {label: 'Save Report', icon: 'pi pi-times', command: () => {
          this.delete();
      }},
      {label: 'Reset', icon: 'pi pi-times', command: () => {
          this.delete();
      }},
      {label: 'Help', icon: 'pi pi-times', command: () => {
          this.delete();
      }},
      {label: 'Download', icon: 'pi pi-times', command: () => {
          this.delete();
      }},
     
      // {label: 'Setup', icon: 'pi pi-cog', routerLink: ['/setup']}
  ];
  }

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Invoice' }];
    this.authservice.whoAmi().subscribe((value) => {
      console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.aclMenuService.setAllMenus(this.userInfo.sidemenus).then(data => {
        console.log("MENU ALL ACL Set");
        this.aclMenuService.getAclMenu(this.router.url).then(dataacl =>{
          if(JSON.stringify(dataacl.acl) === "{}"){
            console.log("No ACL Founded")
          } else {
            console.log("ACL Founded");
            console.log(dataacl.acl);
              this.usractionbtn[1] = dataacl.acl.read;
              this.usractionbtn[3] = dataacl.acl.delete;
              this.usractionbtn[4] = dataacl.acl.view;
          }
        });
      });
      
    });
    this.refreshingUser();
  }
  refreshingUser() {
    this.isFetching = true;
    this.authservice.whoAmi().subscribe((data: BackendResponse) => {
      // console.log(">>>>>>> "+JSON.stringify(data));
    
      if ((data.status = 200)) {
        this.userService.retriveUsers().subscribe((orgall: BackendResponse) => {
          // console.log('>>>>>>> ' + JSON.stringify(orgall));
          this.userlist = orgall.data;
          if (this.userlist.length < 1) {
            let objtmp = {
              fullname: 'No records',
              userid: 'No records',
              active: 'No records',
              groupname: 'No records',
              created_at: 'No records',
              updated_at: 'No records',
              description: 'No records',
              bioemailactive: 'No records',
            };
            this.userlist = [];
            this.userlist.push(objtmp);
          }
          this.isFetching = false;
        });
      }
    });
  }
  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedUser = data;
  }

  viewData(data: any) {
    console.log(data);
    this.viewDisplay = true;
    this.selectedUser = data;
  }

  approvalData(data: any) {
    // console.log(data);
    this.viewApprove = true;
    this.selectedUser = data;
    
  }
  approvalSubmit(status){
    // console.log(this.selectedUser);
    // console.log(status);
    let payload = {
      id: this.selectedUser.id,
      oldactive:this.selectedUser.active,
      isactive: status,
      idapproval: this.selectedUser.idapproval
    };
    // console.log('>>>>>>>> payload ' + JSON.stringify(payload));
    this.userService
      .updatebyAdminActive(payload)
      .subscribe((result: BackendResponse) => {
        // console.log(">>>>>>>> return "+JSON.stringify(result));
        if (result.status === 200) {
          this.refreshingUser();
          this.viewApprove = false;
        }
      });
  }

  deleteUser() {
    console.log(this.selectedUser);
    let user = this.selectedUser;
    const payload = { user };
    this.userService
      .deleteUserByAdmin(payload)
      .subscribe((resp: BackendResponse) => {
        console.log(resp);
        if (resp.status === 200) {
          this.showTopSuccess(resp.data);
        }
        this.display = false;
        this.refreshingUser();
      });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }

  save(severity: string) {
    this.messageService.add({severity:severity, summary:'Success', detail:'Data Saved'});
  }

  update() {
      this.messageService.add({severity:'success', summary:'Success', detail:'Data Updated'});
  }

  delete() {
      this.messageService.add({severity:'success', summary:'Success', detail:'Data Deleted'});
  }
}

