import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemjitdetailComponent } from './itemjitdetail.component';

describe('ItemjitdetailComponent', () => {
  let component: ItemjitdetailComponent;
  let fixture: ComponentFixture<ItemjitdetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemjitdetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemjitdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
