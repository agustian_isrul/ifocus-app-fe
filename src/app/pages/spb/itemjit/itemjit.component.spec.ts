import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemjitComponent } from './itemjit.component';

describe('ItemjitComponent', () => {
  let component: ItemjitComponent;
  let fixture: ComponentFixture<ItemjitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemjitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemjitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
