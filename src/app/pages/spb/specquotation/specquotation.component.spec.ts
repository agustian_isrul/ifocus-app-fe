import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecquotationComponent } from './specquotation.component';

describe('SpecquotationComponent', () => {
  let component: SpecquotationComponent;
  let fixture: ComponentFixture<SpecquotationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecquotationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecquotationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
