import { Component, OnInit } from '@angular/core';
import { MenuItem, PrimeNGConfig, TreeNode } from 'primeng/api';
import { Customerr, Product, Representative } from 'src/app/services/spb/customer';
import { CustomerService } from 'src/app/services/spb/customer.service';

@Component({
  selector: 'app-planningorder',
  templateUrl: './planningorder.component.html',
  styleUrls: ['./planningorder.component.scss']
})
export class PlanningorderComponent implements OnInit {
  customers: Customerr[];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  rowGroupMetadata: any;

  statuses: any[];
  
  constructor(
    private customerService: CustomerService
  ) { }

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home'};
    this.breadcrumbs = [
      { label: 'Planning Order'}
    ]
    this.statuses = [
      {label: 'New', value: 'new'},
      {label: 'Cancelled', value: 'cancelled'},
      {label: 'Completed', value: 'completed'},
      {label: 'Not Accept', value: 'notaccept'},
      {label: 'To Be Processed', value: 'tbprocessed'},
      {label: 'Processed', value: 'processed'}
  ]

        this.customerService.getCustomersMediumm().then(data => {
          this.customers = data;
          this.updateRowGroupMetaData();
      });
  }

  onSort() {
      this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
      this.rowGroupMetadata = {};

      if (this.customers) {
          for (let i = 0; i < this.customers.length; i++) {
              let rowData = this.customers[i];
              let representativeName = rowData.representative.name;
              
              if (i == 0) {
                  this.rowGroupMetadata[representativeName] = { index: 0, size: 1 };
              }
              else {
                  let previousRowData = this.customers[i - 1];
                  let previousRowGroup = previousRowData.representative.name;
                  if (representativeName === previousRowGroup)
                      this.rowGroupMetadata[representativeName].size++;
                  else
                      this.rowGroupMetadata[representativeName] = { index: i, size: 1 };
              }
          }
      }
  }
  
}


