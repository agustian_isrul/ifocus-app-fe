import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningorderComponent } from './planningorder.component';

describe('PlanningorderComponent', () => {
  let component: PlanningorderComponent;
  let fixture: ComponentFixture<PlanningorderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanningorderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
