import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForumthreadComponent } from './forumthread.component';

describe('ForumthreadComponent', () => {
  let component: ForumthreadComponent;
  let fixture: ComponentFixture<ForumthreadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForumthreadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumthreadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
