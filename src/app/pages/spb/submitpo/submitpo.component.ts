import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import {FileUploadModule} from 'primeng/fileupload';
import {HttpClientModule} from '@angular/common/http';

@Component({
  selector: 'app-submitpo',
  templateUrl: './submitpo.component.html',
  styleUrls: ['./submitpo.component.scss']
})
export class SubmitpoComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;

  uploadedFiles: any[] = [];
  selectedFile: any;

  constructor( 
    private messageService: MessageService
    ) { }

  ngOnInit( ): void {
    this.home = {icon: 'pi pi-home', routerLink: '/mgm/home'};
  this.breadcrumbs = [
    { label: 'Submit PO' }
  ];

 
    }
    handleChange(event: any): void {
      console.log('Info : Come From handlechange');
      console.log(event.target.files);
    }
  }

