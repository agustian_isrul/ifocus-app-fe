import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitpoComponent } from './submitpo.component';

describe('SubmitpoComponent', () => {
  let component: SubmitpoComponent;
  let fixture: ComponentFixture<SubmitpoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubmitpoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitpoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
