import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

interface Report {
  name: string
}

@Component({
  selector: 'app-requisition',
  templateUrl: './requisition.component.html',
  styleUrls: ['./requisition.component.scss']
})
export class RequisitionComponent implements OnInit {

  breadcrumbs!: MenuItem[];
  home!: MenuItem;

  reports: Report[];

  selectedReport: Report;

  constructor() {
    this.reports = [
      {name: 'All'},
      {name: 'Open'},
      {name: 'Close',},
  ];
   }

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Requisition' }];
  }

}
