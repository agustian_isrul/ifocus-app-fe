import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

interface Supplier {
  name : string,
  value : string
}

interface Tahun {
  nomor : number;
}

@Component({
  selector: 'app-suppliermaster',
  templateUrl: './suppliermaster.component.html',
  styleUrls: ['./suppliermaster.component.scss']
})
export class SuppliermasterComponent implements OnInit {
  
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching:boolean = false;

  supplier: Supplier[];
  selectedSupplier: Supplier;

  tahun: Tahun[];
  selectedTahun: Tahun;

  constructor() { }

  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/mgm/home'};
    this.breadcrumbs = [
      { label: 'Supplier Master' }
    ];
    this.supplier = [
      {name: 'ALLFORM INDONESIA, PT', value: 'ai'},
      {name: 'ALTINEX, PT', value: 'al'},
      {name: 'ANCOL TERANG METAL PRINTING INDUSTRI, PT', value:'atmpi'},
      {name: 'ANINDOJAYA SWAKARYA, PT', value: 'as'},
      {name: 'ARISTROCAT CIPTA MANDIRI, PT', value: 'acm'}
  ];
  
  this.tahun = [
    {nomor: 2009},
    {nomor: 2010},
    {nomor: 2011},
    {nomor: 2012},
    {nomor: 2013}
  ];
  }

}
