import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';


interface Type {
  name: string
}

@Component({
  selector: 'app-ponumberjit',
  templateUrl: './ponumberjit.component.html',
  styleUrls: ['./ponumberjit.component.scss']
})

export class PonumberjitComponent implements OnInit {

  typevalue: Type[];
  type: any = [];
  typeselected: any = [];
  suppliervalue: [];
  supplier: any = [];
  supplierselected: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  scrollheight:any ="400px"

  itemjitlist: any[] = [];
  addbtn = { route: 'detail', label: 'Create JIT' };

  itemjitheader: any = [
    { label: 'Item Number', sort: 'item_number' },
    { label: 'Description', sort: 'description' },
    { label: 'UoM', sort: 'uom' },
    { label: 'Pack Size', sort: 'pack_size' },
    { label: 'Category', sort: 'category' },
    { label: 'Vendor Name', sort: 'vendor_name' },
    { label: 'ROP Vendor', sort: 'rop_vendor' },
    { label: 'ROP Multiple', sort: 'rop_multiple' },
    { label: 'Lead Time', sort: 'lead_time' },
  ];
  itemjitcolname: any = [
    'item_number',
    'description',
    'uom',
    'pack_size',
    'category',
    'vendor_name',
    'rop_vendor',
    'rop_multiple',
    'lead_time',
  ];
  itemjitcolhalign: any = [
    '',
    '',
    'p-text-center',
    '',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  // itemjitcolwidth: any = [
  //   '',
  //   '',
  //   { width: '110px' },
  //   { width: '170px' },
  //   { width: '110px' },
  //   { width: '170px' },
  // ];
  itemjitcolwidth: any = [
    '',
    '',
    '',
    '',
    { width: '170px' },
    '',
    '',
    '',
    '',
  ];
  // orgcollinghref:any = {'url':'#','label':'Application'}
  itemjitactionbtn: any = [1, 1, 1, 1, 1, 1, 1, 1,];
  constructor() {
    this.typevalue = [
      {name: 'Daily'},
      {name: 'Weekly',},
    ];
  }

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'PO Number JIT' }];
   
  }

  search(){

  }

  clearDropdown(){
    this.typeselected = [];
    this.supplierselected = [];
  }
}
