import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PonumberjitComponent } from './ponumberjit.component';

describe('PonumberjitComponent', () => {
  let component: PonumberjitComponent;
  let fixture: ComponentFixture<PonumberjitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PonumberjitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PonumberjitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
