import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CutoffcustomeroutletpanelComponent } from './cutoffcustomeroutletpanel.component';

describe('CutoffcustomeroutletpanelComponent', () => {
  let component: CutoffcustomeroutletpanelComponent;
  let fixture: ComponentFixture<CutoffcustomeroutletpanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CutoffcustomeroutletpanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CutoffcustomeroutletpanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
