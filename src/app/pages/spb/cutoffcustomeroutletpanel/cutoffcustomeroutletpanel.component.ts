import { Component, OnInit } from '@angular/core';
import { MenuItem, Message, MessageService } from 'primeng/api';
import { CustomerPanelService } from 'src/app/services/spb/customer-panel.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';

interface Dropdown {
  name: string,
  value: string
}


interface CustomerOutlet {
  outletId: string,
  outletName: string,
  address: string,
  area: string,
  channel: string,
  areaLine: string,
  startDate: string
  endDate: string
}

@Component({
  selector: 'app-cutoffcustomeroutletpanel',
  templateUrl: './cutoffcustomeroutletpanel.component.html',
  styleUrls: ['./cutoffcustomeroutletpanel.component.scss']
})

export class CutoffcustomeroutletpanelComponent implements OnInit {
  
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching: boolean = false;
  tokenID: string = '';
  display = false;


  ypeselected: any = [];
  areaLineList: Dropdown[];
  chanelList: Dropdown[];
  dialogCustomer: boolean;
  customerOutletList : CustomerOutlet[];

  selectAllOutlet: boolean = false;
  selectedCustomersOutlet: any[];

  areaLineSelected:any = {};
  channelselected:any = {};

  constructor(
    private customerPanelService : CustomerPanelService,
  ) { }

  ngOnInit(): void {

    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Cutt Customer Outlet panel' }];

    this.areaLineList = [
      {name: 'Dki Jakarta', value: 'jakarta'},
      {name: 'Jawa Barat', value: 'jawabarat'},
      {name: 'Jawa Tengah', value: 'jawatengah'},
      {name: 'Kalimantan', value: 'kalimanatn'}
    ];
    this.chanelList = [
      {name: 'Chanel 1', value: 'chanel1'},
      {name: 'chanel 2', value: 'chanel2'},
      {name: 'Chanel 3', value: 'chanel3'},
      {name: 'Chanel 4', value: 'chanel4'}
    ];

  }

  onSelectionChangeOutlet(value = []) {
    this.selectAllOutlet = value.length === this.customerOutletList.length;
    this.selectedCustomersOutlet = value;
  }

  onSelectAllChangeOutlet(event) {
      const checked = event.checked;    
  }

  btnSearch() {

    this.customerOutletList = [];
    this.customerPanelService.getCutCustomerPanel().subscribe((orgall: BackendResponse) => {
    
      this.customerOutletList = orgall.data;
      
      if (this.customerOutletList.length < 1) {
        
      }

      this.isFetching = false;
    });
  }

  btnCutOff() {
    console.log("tets");
  }

}
