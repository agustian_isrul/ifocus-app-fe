import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BackmenulayoutComponent } from './backmenulayout.component';

describe('BackmenulayoutComponent', () => {
  let component: BackmenulayoutComponent;
  let fixture: ComponentFixture<BackmenulayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BackmenulayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BackmenulayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
