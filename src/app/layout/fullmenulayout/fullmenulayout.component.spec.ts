import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FullmenulayoutComponent } from './fullmenulayout.component';

describe('FullmenulayoutComponent', () => {
  let component: FullmenulayoutComponent;
  let fixture: ComponentFixture<FullmenulayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FullmenulayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FullmenulayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
