(function (window) {
  // console.log(process.env.API_URL);
  window.env = window.env || {};

  // Environment variables
  // window["env"]["apiUrl"] = "http://localhost:9005/";
  // window["env"]["baseUrl"] = "http://localhost:9004/";
  // window["env"]["apiUrl"] = "http://182.169.41.153:3000/";
  // window["env"]["baseUrl"] = "http://182.169.41.153:3013/";
  window["env"]["apiUrl"] = "http://localhost:3000/";
  window["env"]["adminUrl"] = "http://localhost:3013/";
  window["env"]["orderUrl"] = "http://localhost:3014/";
  window["env"]["reportUrl"] = "http://localhost:3015/";
  window["env"]["supmanUrl"] = "http://localhost:3016/";
  window["env"]["bindbatchUrl"] = "http://localhost:3017/";
})(this);
